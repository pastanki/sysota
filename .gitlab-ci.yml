# SPDX-License-Identifier: Apache-2.0
# SPDX-FileCopyrightText: Huawei Inc.

stages:
  - compliance
  - test
  - build
  - integration

include:
  - template: 'Workflows/MergeRequest-Pipelines.gitlab-ci.yml'
  - remote: 'https://gitlab.com/zygoon/go-gitlab-ci/-/raw/master/go.yml'

.go:
  variables:
    # The project requires Go 1.16 compatibility.
    # For Yocto Kirkstone the baseline will move to Go 1.18.
    CI_GO_VERSION: "1.16"

dco:
  interruptible: true
  stage: compliance
  image: christophebedard/dco-check:latest
  rules:
    - if: $CI_MERGE_REQUEST_ID
    - if: $CI_EXTERNAL_PULL_REQUEST_IID
    - if: $CI_COMMIT_BRANCH == '$CI_DEFAULT_BRANCH'
  script:
    # Work around a bug in dco-check affecting pipelines for merge requests.
    # https://github.com/christophebedard/dco-check/issues/104
    - |
        if [ "${CI_MERGE_REQUEST_EVENT_TYPE:-}" = detached ]; then
            git fetch -a  # so that we can resolve branch names below
            export CI_COMMIT_BRANCH="$CI_COMMIT_REF_NAME";
            export CI_COMMIT_BEFORE_SHA="$CI_MERGE_REQUEST_DIFF_BASE_SHA";
            export CI_MERGE_REQUEST_SOURCE_BRANCH_SHA="$(git rev-parse "origin/$CI_MERGE_REQUEST_SOURCE_BRANCH_NAME")";
            export CI_MERGE_REQUEST_TARGET_BRANCH_SHA="$(git rev-parse "origin/$CI_MERGE_REQUEST_TARGET_BRANCH_NAME")";
        fi
    - dco-check --default-branch-from-remote --verbose

reuse:
  interruptible: true
  stage: compliance
  tags: [x86_64]  # https://twitter.com/zygoon/status/1474746295088787458
  image:
    name: fsfe/reuse:latest
    entrypoint: [""]
  script:
    - reuse lint

go-test:
  tags: [$RUNNER_OS, $RUNNER_ARCH]
  parallel:
    matrix:
      - RUNNER_OS: linux
        RUNNER_ARCH: [x86_64, aarch64, armv7l]
        CI_GO_VERSION: ["1.16", latest]
  before_script:
    # Set up caching.
    - !reference [.go, before_script]
    - echo -e "\e[0Ksection_start:$(date +%s):install_debian[collapsed=true]\r\e[0KInstalling Debian dependencies..."
    # Install dbus-daemon and squashfs-tools which are used by dbus and
    # squashfs-tools tests. The golang:latest image is based on Debian so we
    # can just apt-get install it.
    - apt-get update && apt-get install -y dbus squashfs-tools
    - echo -e "\e[0Ksection_end:$(date +%s):install_debian\r\e[0K"
    # Install go package for emitting GitLab friendly coverage information.
    - echo -e "\e[0Ksection_start:$(date +%s):install_cover\r\e[0KInstalling Go coverage dependencies..."
    - go install github.com/boumenot/gocover-cobertura@latest
    - echo -e "\e[0Ksection_end:$(date +%s):install_cover\r\e[0K"
  script:
    - echo -e "\e[0Ksection_start:$(date +%s):test\r\e[0KTesting..."
    - go test $(case $(uname -m) in x86_64|aarch64) echo -race ;; esac) -coverprofile=coverage.txt -covermode atomic ./...
    - echo -e "\e[0Ksection_end:$(date +%s):test\r\e[0K"
    - echo -e "\e[0Ksection_start:$(date +%s):test_coverage[collapsed=true]\r\e[0KGenerating coverage report..."
    - go tool cover -func=coverage.txt
    - $(go env GOPATH)/bin/gocover-cobertura <coverage.txt >"$CI_PROJECT_DIR"/coverage.xml
    - echo -e "\e[0Ksection_end:$(date +%s):test_coverage\r\e[0K"
  artifacts:
    paths:
      - coverage.xml
    reports:
      cobertura: coverage.xml

.build-with-kaniko:
  stage: build
  # This is artificially limited because we don't handle build tags correctly
  # and the other side of this job requires x86_64 (for spread).
  tags: [x86_64]
  image:
    name: gcr.io/kaniko-project/executor:debug
    entrypoint: [""]
  script:
    - |
      set -xe
      mkdir -p /kaniko/.docker
      printf '{"auths":{"%s":{"auth":"%s"}}}\n' "$CI_REGISTRY" "$(printf '%s:%s' "$CI_REGISTRY_USER" "$CI_REGISTRY_PASSWORD" | base64)" > /kaniko/.docker/config.json
      BUILD_DATE="$(date '+%FT%T%z' | sed -E -n 's/(\+[0-9]{2})([0-9]{2})$/\1:\2/p')" #rfc 3339 date
      BUILD_TITLE=$(echo "$CI_PROJECT_TITLE" | tr " " "_")
      IMAGE_LABELS="$(cat <<EOM
          --label build-date=$BUILD_DATE
          --label com.gitlab.ci.cijoburl=$CI_JOB_URL
          --label com.gitlab.ci.commiturl=$CI_PROJECT_URL/commit/$CI_COMMIT_SHA
          --label com.gitlab.ci.email=$GITLAB_USER_EMAIL
          --label com.gitlab.ci.mrurl=$CI_PROJECT_URL/-/merge_requests/$CI_MERGE_REQUEST_ID
          --label com.gitlab.ci.pipelineurl=$CI_PIPELINE_URL
          --label com.gitlab.ci.tagorbranch=$CI_COMMIT_REF_NAME
          --label com.gitlab.ci.user=$CI_SERVER_URL/$GITLAB_USER_LOGIN
          --label org.opencontainers.image.authors=$CI_SERVER_URL/$GITLAB_USER_LOGIN
          --label org.opencontainers.image.created=$BUILD_DATE
          --label org.opencontainers.image.description=$BUILD_TITLE
          --label org.opencontainers.image.documentation=$CI_PROJECT_URL
          --label org.opencontainers.image.licenses=$CI_PROJECT_URL
          --label org.opencontainers.image.ref.name=$CI_REGISTRY_IMAGE:$CI_COMMIT_REF_NAME
          --label org.opencontainers.image.revision=$CI_COMMIT_SHA
          --label org.opencontainers.image.source=$CI_PROJECT_URL
          --label org.opencontainers.image.title=$BUILD_TITLE
          --label org.opencontainers.image.url=$CI_PROJECT_URL
          --label org.opencontainers.image.vendor=$CI_SERVER_URL/$GITLAB_USER_LOGIN
          --label org.opencontainers.image.version=$CI_COMMIT_TAG
          --label vcs-url=$CI_PROJECT_URL
      EOM
      )"

      ADDITIONAL_TAG_LIST="$CI_COMMIT_REF_NAME $CI_COMMIT_SHORT_SHA"
      if [ "$CI_COMMIT_BRANCH" = "$CI_DEFAULT_BRANCH" ]; then
          ADDITIONAL_TAG_LIST="$ADDITIONAL_TAG_LIST latest";
      fi

      if [ -n "$ADDITIONAL_TAG_LIST" ]; then
          for TAG in $ADDITIONAL_TAG_LIST; do
              FORMATTED_TAG_LIST="$FORMATTED_TAG_LIST --tag $CI_REGISTRY_IMAGE:$TAG "
          done
      fi
      FORMATTED_TAG_LIST="$(echo "$FORMATTED_TAG_LIST" | sed -e 's/--tag/--destination/g')"

      echo "Building and shipping image to $CI_REGISTRY_IMAGE"
      exec /kaniko/executor --context "$CI_PROJECT_DIR/$CONTAINER_PATH" --dockerfile "$CI_PROJECT_DIR/$CONTAINER_PATH/Dockerfile" --destination $CI_REGISTRY_IMAGE/$CONTAINER_PATH $IMAGE_LABELS
  rules:
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
      changes:
        - $CONTAINER_PATH/*
    - if: '$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH'
      changes:
        - $CONTAINER_PATH/*

spell:
  interruptible: true
  stage: test
  image:
    name: registry.booting.oniroproject.org/distro/oniro/npm-cspell:latest
  tags: [x86_64]  # TODO(zyga): publish a container ourselves or switch to node containers.
  script:
    # Note that the particular options are duplicated with what's defined in
    # Makefile. To avoid this we'd have to get zmk as an installable dependency
    # and replace everything below with "make static-check -j" but this is
    # somewhat complex given the starting docker image.
    - "cspell lint '**/*.go' '**/*.md' '**/*.yaml' '**/*.json' '**/*.txt' '**/*.log' '**/*.mk' Makefile '.tours/*.tour' 'man/*.in'"

docker-spread:
  extends: .build-with-kaniko
  variables:
    CONTAINER_PATH: docker/spread

spread-qemu:
  interruptible: true
  # TODO(zyga): depend on amd64/linux cross build explicitly when
  # GitLab supports this: https://gitlab.com/gitlab-org/gitlab/-/issues/254821
  stage: integration
  needs:
    - job: go-test
      artifacts: false
  image: registry.booting.oniroproject.org/distro/components/sysota/docker/spread:latest
  tags: [kvm, x86_64]  # TODO(zyga): build this container locally, add aarch64 support
  variables:
    DEBIAN_FRONTEND: noninteractive
    SPREAD_QEMU_KVM: "1"
    TRANSFER_METER_FREQUENCY: "2s"
    ARTIFACT_COMPRESSION_LEVEL: "fast"
    CACHE_COMPRESSION_LEVEL: "fastest"
  parallel:
    matrix:
      - VERSION_CODENAME: [impish]
        ID: [ubuntu]
        ID_VERSION: ["21.10"]
      - VERSION_CODENAME: [jammy]
        ID: [ubuntu]
        ID_VERSION: ["22.04"]
  before_script:
    - mkdir -p .cache
    - mkdir -p ~/.spread/qemu
    - |
      set -ex
      if [ ! -e .cache/autopkgtest-$VERSION_CODENAME-amd64.img ]; then
        # Prepare a qemu image that is useful for testing. For now this is just
        # a vanilla Ubuntu image with some packages pre-installed to speed up
        # the project prepare step in spread.yaml.
        cd .cache
        time autopkgtest-buildvm-ubuntu-cloud -r $VERSION_CODENAME --post-command "apt-get install ssh golang-go zmk rauc-service squashfs-tools jq && apt-get install -d postgresql"
        cd ..
      fi
    - ln -s $CI_PROJECT_DIR/.cache/autopkgtest-$VERSION_CODENAME-amd64.img ~/.spread/qemu/$ID-$ID_VERSION-64.img
  script:
    - 'time spread -v qemu:$ID-$ID_VERSION-64:'
  cache:
    # If the image needs to be rebuilt, just bump this number.
    key: autopkgtest-image-$VERSION_CODENAME-7
    # The image compresses well. Don't compress it here, GitLab will compress
    # it automatically as a part of the cache creation step.
    paths:
     - .cache/autopkgtest-$VERSION_CODENAME-amd64.img
