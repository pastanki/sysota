// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

package boot_test

import (
	. "gopkg.in/check.v1"

	"gitlab.com/zygoon/sysota/boot"
)

type cmdLineSuite struct{}

var _ = Suite(&cmdLineSuite{})

func (s *cmdLineSuite) TestFlags(c *C) {
	var cl boot.CmdLine

	cl.SetFlag("foo")
	c.Check(string(cl), Equals, "foo")

	cl.SetFlag("bar")
	c.Check(string(cl), Equals, "foo bar")

	cl.SetFlag("foo")
	c.Check(string(cl), Equals, "foo bar")

	c.Check(cl.HasFlag("foo"), Equals, true)
	c.Check(cl.HasFlag("bar"), Equals, true)
	c.Check(cl.HasFlag("froz"), Equals, false)

	cl.DelFlag("foo")
	c.Check(string(cl), Equals, "bar")

	cl.DelFlag("froz")
	c.Check(string(cl), Equals, "bar")

	cl.DelFlag("bar")
	c.Check(string(cl), Equals, "")
}

func (s *cmdLineSuite) TestArgs(c *C) {
	var cl boot.CmdLine

	cl.SetArg("foo", "value")
	c.Check(string(cl), Equals, "foo=value")

	cl.SetArg("bar", "")
	c.Check(string(cl), Equals, "foo=value bar=")

	cl.SetArg("foo", "new-value")
	c.Check(string(cl), Equals, "foo=new-value bar=")

	c.Check(cl.ArgValues("foo"), DeepEquals, []string{"new-value"})
	c.Check(cl.ArgValues("bar"), DeepEquals, []string{""})
	c.Check(cl.ArgValues("froz"), DeepEquals, []string(nil))

	cl.DelArg("foo")
	c.Check(string(cl), Equals, "bar=")

	cl.DelArg("froz")
	c.Check(string(cl), Equals, "bar=")

	cl.DelArg("bar")
	c.Check(string(cl), Equals, "")
}

func (s *cmdLineSuite) TestArgValue(c *C) {
	cl := boot.CmdLine("foo=value bar= quux=value-1 quux=value-2")

	val, err := cl.ArgValue("foo")
	c.Assert(err, IsNil)
	c.Check(val, Equals, "value")

	val, err = cl.ArgValue("bar")
	c.Assert(err, IsNil)
	c.Check(val, Equals, "")

	_, err = cl.ArgValue("froz")
	c.Assert(err, ErrorMatches, `argument not found`)

	_, err = cl.ArgValue("quux")
	c.Assert(err, ErrorMatches, `argument has many values`)
}
