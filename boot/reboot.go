// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

package boot

import "os/exec"

const rebootCommand = "reboot"

// Reboot gracefully reboots the system.
//
// Additional arguments are passed to the system reboot utility.
func Reboot(args ...string) error {
	cmd := exec.Command(rebootCommand, args...)
	return cmd.Run()
}
