// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

package boot_test

import (
	. "gopkg.in/check.v1"

	"gitlab.com/zygoon/sysota/boot"
	"gitlab.com/zygoon/sysota/pkg/testutil"
)

type rebootSuite struct {
	testutil.CommandSuite
}

var _ = Suite(&rebootSuite{})

func (s *rebootSuite) TestReboot(c *C) {
	restore, rebootLog := s.MockCommand(c, "reboot")
	defer restore()

	err := boot.Reboot()
	c.Assert(err, IsNil)
	c.Check(rebootLog(c), DeepEquals, []string{`reboot`})

	err = boot.Reboot("potato")
	c.Assert(err, IsNil)
	c.Check(rebootLog(c), DeepEquals, []string{`reboot potato`})

	err = boot.Reboot("0 tryboot")
	c.Assert(err, IsNil)
	c.Check(rebootLog(c), DeepEquals, []string{`reboot 0\ tryboot`})
}
