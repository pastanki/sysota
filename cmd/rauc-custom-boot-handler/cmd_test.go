// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

package rauccustomboothandler_test

import (
	"context"
	"errors"
	"flag"
	"testing"

	"github.com/godbus/dbus/v5"
	"gitlab.com/zygoon/go-cmdr"
	"gitlab.com/zygoon/go-cmdr/router"
	. "gopkg.in/check.v1"

	"gitlab.com/zygoon/sysota/boot"
	rauccustomboothandler "gitlab.com/zygoon/sysota/cmd/rauc-custom-boot-handler"
	"gitlab.com/zygoon/sysota/cmd/sysotad/raucboothosted"
	svcspec "gitlab.com/zygoon/sysota/cmd/sysotad/spec"
	"gitlab.com/zygoon/sysota/pkg/dbusutil"
	"gitlab.com/zygoon/sysota/pkg/dbusutil/dbustest"
	"gitlab.com/zygoon/sysota/pkg/testutil"
	"gitlab.com/zygoon/sysota/rauc/boothandler/boottest"
)

func Test(t *testing.T) { TestingT(t) }

type cmdSuite struct {
	dbustest.Suite
	conn        *dbus.Conn
	serviceHost *dbusutil.ServiceHost
	handler     boottest.Handler
}

var _ = Suite(&cmdSuite{})

func (s *cmdSuite) TearDownTest(c *C) {
	if s.serviceHost != nil {
		err := s.serviceHost.Unexport()
		c.Check(err, IsNil)

		s.serviceHost = nil
	}

	if s.conn != nil {
		err := s.conn.Close()
		c.Check(err, IsNil)

		s.conn = nil
	}
}

func (s *cmdSuite) EnsureConn(c *C) *dbus.Conn {
	if s.conn == nil {
		var err error
		s.conn, err = dbusutil.SystemBus()
		c.Assert(err, IsNil)
	}

	return s.conn
}

func (s *cmdSuite) SetUpTest(_ *C) {
	s.handler.ResetCallbacks()
}

func (s *cmdSuite) EnsureService(c *C) {
	if s.serviceHost == nil {
		conn := s.EnsureConn(c)

		s.serviceHost = dbusutil.NewServiceHost(conn)
		s.serviceHost.AddHostedService(raucboothosted.New(&s.handler))

		err := s.serviceHost.Export()
		c.Assert(err, IsNil)

		reply, err := conn.RequestName(svcspec.BusName, dbus.NameFlagDoNotQueue)
		c.Assert(err, IsNil)
		c.Assert(reply, Equals, dbus.RequestNameReplyPrimaryOwner)
	}
}

func (s *cmdSuite) TestRunInsufficientArgs(c *C) {
	ctx, stdout, stderr := testutil.BufferOutput(context.TODO())
	err := rauccustomboothandler.Cmd{}.Run(ctx, []string{})

	c.Assert(errors.Is(err, router.ErrExpectedCmd), Equals, true)
	c.Check(stdout.String(), Equals, "")
	c.Check(stderr.String(), Equals, ""+
		"Usage: rauc-custom-boot-handler COMMAND [...]\n"+
		"\n"+
		"Available commands:\n"+
		"  get-primary  get the name of the primary slot\n"+
		"  get-state    get the state of a given slot\n"+
		"  set-primary  set the name of the primary slot\n"+
		"  set-state    set the state of a given slot\n")
}

func (s *cmdSuite) TestHelp(c *C) {
	ctx, stdout, stderr := testutil.BufferOutput(context.TODO())
	err := rauccustomboothandler.Cmd{}.Run(ctx, []string{"--help"})

	c.Assert(errors.Is(err, flag.ErrHelp), Equals, true)
	c.Check(stdout.String(), Equals, "")
	c.Check(stderr.String(), Equals, ""+
		"Usage: rauc-custom-boot-handler COMMAND [...]\n"+
		"\n"+
		"Available commands:\n"+
		"  get-primary  get the name of the primary slot\n"+
		"  get-state    get the state of a given slot\n"+
		"  set-primary  set the name of the primary slot\n"+
		"  set-state    set the state of a given slot\n")
}

func (s *cmdSuite) TestUnknownOptions(c *C) {
	ctx, stdout, stderr := testutil.BufferOutput(context.TODO())
	err := rauccustomboothandler.Cmd{}.Run(ctx, []string{"-potato"})

	c.Assert(err, ErrorMatches, "flag provided but not defined: -potato")
	c.Check(stdout.String(), Equals, "")
	c.Check(stderr.String(), Equals, ""+
		"flag provided but not defined: -potato\n"+
		"Usage: rauc-custom-boot-handler COMMAND [...]\n"+
		"\n"+
		"Available commands:\n"+
		"  get-primary  get the name of the primary slot\n"+
		"  get-state    get the state of a given slot\n"+
		"  set-primary  set the name of the primary slot\n"+
		"  set-state    set the state of a given slot\n")
}

func (s *cmdSuite) TestRunSmoke(c *C) {
	s.EnsureService(c)

	// Just a smoke test, we don't repeat all the tests from boothandler package here.
	var called testutil.CallWitness

	s.handler.MockSlotStateFn(func(slot boot.Slot) (boot.SlotState, error) {
		called.Witness()

		c.Check(slot, Equals, boot.SlotA)

		return boot.GoodSlot, nil
	})

	ctx, stdout, stderr := testutil.BufferOutput(context.TODO())
	err := rauccustomboothandler.Cmd{}.Run(ctx, []string{"get-state", "A"})

	c.Assert(err, IsNil)
	c.Check(called.Once(), Equals, true)
	c.Check(stdout.String(), Equals, "good\n")
	c.Check(stderr.String(), Equals, "")
}

type brokenDBusSuite struct{}

var _ = Suite(&brokenDBusSuite{})

func (s *brokenDBusSuite) TestRunButNoSystemBus(c *C) {
	restoreDBus, err := dbustest.SetDBusSystemBusAddress("unix:path=potato")
	c.Assert(err, IsNil)

	defer func() { c.Assert(restoreDBus(), IsNil) }()

	ctx, stdout, stderr := testutil.BufferOutput(context.TODO())
	err = rauccustomboothandler.Cmd{}.Run(ctx, []string{"get-primary"})

	c.Assert(err, ErrorMatches, `dial .*potato: connect: no such file or directory`)
	c.Check(stdout.String(), Equals, "")
	c.Check(stderr.String(), Equals, "")
}

var errBoom = errors.New("boom")

type suiteBase struct {
	handler boottest.Handler
	cmd     cmdr.Cmd
}

func (s *suiteBase) SetUpTest(_ *C) {
	s.handler.ResetCallbacks()
	s.cmd = rauccustomboothandler.InterfaceCmd{&s.handler}
}

type routeSuite struct {
	suiteBase
}

var _ = Suite(&routeSuite{})

func (s *routeSuite) TestUnknownCommand(c *C) {
	ctx, stdout, stderr := testutil.BufferOutput(context.TODO())
	err := s.cmd.Run(ctx, []string{"potato"})

	c.Assert(err, ErrorMatches, `unknown command`)
	c.Check(stdout.String(), Equals, "")
	c.Check(stderr.String(), Equals, ""+
		"Usage: rauc-custom-boot-handler COMMAND [...]\n"+
		"\n"+
		"Available commands:\n"+
		"  get-primary  get the name of the primary slot\n"+
		"  get-state    get the state of a given slot\n"+
		"  set-primary  set the name of the primary slot\n"+
		"  set-state    set the state of a given slot\n")
}

// get-primary tests

func (s *routeSuite) TestGetPrimarySuccess(c *C) {
	s.handler.MockPrimarySlotFn(func() (boot.Slot, error) { return boot.SlotA, nil })

	ctx, stdout, stderr := testutil.BufferOutput(context.TODO())
	err := s.cmd.Run(ctx, []string{"get-primary"})
	c.Assert(err, IsNil)

	c.Check(stdout.String(), Equals, "A\n")
	c.Check(stderr.String(), Equals, "")
}

func (s *routeSuite) TestGetPrimaryFailure(c *C) {
	s.handler.MockPrimarySlotFn(func() (boot.Slot, error) { return boot.InvalidSlot, errBoom })
	err := s.cmd.Run(context.TODO(), []string{"get-primary"})
	c.Assert(err, ErrorMatches, `boom`)
}

func (s *routeSuite) TestGetPrimaryExtraArgs(c *C) {
	err := s.cmd.Run(context.TODO(), []string{"get-primary", "foo"})
	c.Assert(err, ErrorMatches, `unexpected argument`)
}

// set-primary tests

func (s *routeSuite) TestSetPrimarySuccess(c *C) {
	called := false

	s.handler.MockSetPrimarySlotFn(func(slot boot.Slot) error {
		called = true

		c.Check(slot, Equals, boot.SlotA)

		return nil
	})

	ctx, stdout, stderr := testutil.BufferOutput(context.TODO())
	err := s.cmd.Run(ctx, []string{"set-primary", "A"})

	c.Assert(err, IsNil)
	c.Check(called, Equals, true)
	c.Check(stdout.String(), Equals, "")
	c.Check(stderr.String(), Equals, "")
}

func (s *routeSuite) TestSetPrimaryFailure(c *C) {
	s.handler.MockSetPrimarySlotFn(func(slot boot.Slot) error {
		return errBoom
	})

	err := s.cmd.Run(context.TODO(), []string{"set-primary", "A"})
	c.Assert(err, ErrorMatches, `boom`)
}

func (s *routeSuite) TestSetPrimaryInvalidSlotName(c *C) {
	err := s.cmd.Run(context.TODO(), []string{"set-primary", "potato"})
	c.Assert(err, ErrorMatches, `invalid boot slot`)
}

func (s *routeSuite) TestSetPrimaryMissingArgs(c *C) {
	err := s.cmd.Run(context.TODO(), []string{"set-primary"})
	c.Assert(err, ErrorMatches, `expected name of the primary slot`)
}

func (s *routeSuite) TestSetPrimaryExtraArgs(c *C) {
	err := s.cmd.Run(context.TODO(), []string{"set-primary", "A", "foo"})
	c.Assert(err, ErrorMatches, `unexpected argument`)
}

// get-state tests

func (s *routeSuite) TestGetStateSuccess(c *C) {
	var called testutil.CallWitness

	s.handler.MockSlotStateFn(func(slot boot.Slot) (boot.SlotState, error) {
		called.Witness()

		c.Assert(slot, Equals, boot.SlotA)

		return boot.GoodSlot, nil
	})

	ctx, stdout, stderr := testutil.BufferOutput(context.TODO())
	err := s.cmd.Run(ctx, []string{"get-state", "A"})

	c.Assert(err, IsNil)
	c.Check(called.Once(), Equals, true)
	c.Check(stdout.String(), Equals, "good\n")
	c.Check(stderr.String(), Equals, "")
}

func (s *routeSuite) TestGetStateFailure(c *C) {
	s.handler.MockSlotStateFn(func(slot boot.Slot) (boot.SlotState, error) {
		c.Assert(slot, Equals, boot.SlotA)
		return boot.InvalidSlotState, errBoom
	})

	err := s.cmd.Run(context.TODO(), []string{"get-state", "A"})
	c.Assert(err, ErrorMatches, `boom`)
}

func (s *routeSuite) TestGetStateInvalidState(c *C) {
	s.handler.MockSlotStateFn(func(slot boot.Slot) (boot.SlotState, error) {
		c.Assert(slot, Equals, boot.SlotB)
		return boot.InvalidSlotState, nil
	})

	ctx, stdout, stderr := testutil.BufferOutput(context.TODO())
	err := s.cmd.Run(ctx, []string{"get-state", "B"})
	c.Assert(err, IsNil)
	// XXX(zyga): this is probably a violation of the protocol
	c.Check(stdout.String(), Equals, "invalid\n")
	c.Check(stderr.String(), Equals, "")
}

func (s *routeSuite) TestGetStateMissingArgs(c *C) {
	ctx, stdout, stderr := testutil.BufferOutput(context.TODO())
	err := s.cmd.Run(ctx, []string{"get-state"})

	c.Assert(err, ErrorMatches, `expected slot name`)
	c.Check(stdout.String(), Equals, "")
	c.Check(stderr.String(), Equals, "")
}

func (s *routeSuite) TestGetStateInvalidSlotName(c *C) {
	ctx, stdout, stderr := testutil.BufferOutput(context.TODO())
	err := s.cmd.Run(ctx, []string{"get-state", "potato"})

	c.Assert(err, ErrorMatches, `invalid boot slot`)
	c.Check(stdout.String(), Equals, "")
	c.Check(stderr.String(), Equals, "")
}

func (s *routeSuite) TestGetStateExtraArgs(c *C) {
	ctx, stdout, stderr := testutil.BufferOutput(context.TODO())
	err := s.cmd.Run(ctx, []string{"get-state", "A", "foo"})

	c.Assert(err, ErrorMatches, `unexpected argument`)
	c.Check(stdout.String(), Equals, "")
	c.Check(stderr.String(), Equals, "")
}

// set-state tests

func (s *routeSuite) TestSetStateSuccess(c *C) {
	var called testutil.CallWitness

	s.handler.MockSetSlotStateFn(func(slot boot.Slot, state boot.SlotState) error {
		called.Witness()

		c.Assert(slot, Equals, boot.SlotA)
		c.Assert(state, Equals, boot.GoodSlot)

		return nil
	})

	ctx, stdout, stderr := testutil.BufferOutput(context.TODO())
	err := s.cmd.Run(ctx, []string{"set-state", "A", "good"})

	c.Assert(err, IsNil)
	c.Check(called.Once(), Equals, true)
	c.Check(stdout.String(), Equals, "")
	c.Check(stderr.String(), Equals, "")
}

func (s *routeSuite) TestSetStateFailure(c *C) {
	s.handler.MockSetSlotStateFn(func(slot boot.Slot, state boot.SlotState) error {
		c.Assert(slot, Equals, boot.SlotA)
		c.Assert(state, Equals, boot.GoodSlot)

		return errBoom
	})

	ctx, stdout, stderr := testutil.BufferOutput(context.TODO())
	err := s.cmd.Run(ctx, []string{"set-state", "A", "good"})

	c.Assert(err, ErrorMatches, `boom`)
	c.Check(stdout.String(), Equals, "")
	c.Check(stderr.String(), Equals, "")
}

func (s *routeSuite) TestSetStateMissingSlotName(c *C) {
	ctx, stdout, stderr := testutil.BufferOutput(context.TODO())
	err := s.cmd.Run(ctx, []string{"set-state"})

	c.Assert(err, ErrorMatches, `expected slot name`)
	c.Check(stdout.String(), Equals, "")
	c.Check(stderr.String(), Equals, "")
}

func (s *routeSuite) TestSetStateMissingSlotState(c *C) {
	ctx, stdout, stderr := testutil.BufferOutput(context.TODO())
	err := s.cmd.Run(ctx, []string{"set-state", "A"})

	c.Assert(err, ErrorMatches, `expected slot state`)
	c.Check(stdout.String(), Equals, "")
	c.Check(stderr.String(), Equals, "")
}

func (s *routeSuite) TestSetStateInvalidSlotName(c *C) {
	ctx, stdout, stderr := testutil.BufferOutput(context.TODO())
	err := s.cmd.Run(ctx, []string{"set-state", "potato", "good"})

	c.Assert(err, ErrorMatches, `invalid boot slot`)
	c.Check(stdout.String(), Equals, "")
	c.Check(stderr.String(), Equals, "")
}

func (s *routeSuite) TestSetStateInvalidSlotState(c *C) {
	ctx, stdout, stderr := testutil.BufferOutput(context.TODO())
	err := s.cmd.Run(ctx, []string{"set-state", "B", "potato"})

	c.Assert(err, ErrorMatches, `invalid slot state`)
	c.Check(stdout.String(), Equals, "")
	c.Check(stderr.String(), Equals, "")
}

func (s *routeSuite) TestSetStateExtraArgs(c *C) {
	ctx, stdout, stderr := testutil.BufferOutput(context.TODO())
	err := s.cmd.Run(ctx, []string{"set-state", "A", "good", "foo"})

	c.Assert(err, ErrorMatches, `unexpected argument`)
	c.Check(stdout.String(), Equals, "")
	c.Check(stderr.String(), Equals, "")
}
