# SPDX-License-Identifier: Apache-2.0
# SPDX-FileCopyrightText: Huawei Inc.

summary: Initial smoke tests
prepare: |
    systemctl stop sysotad.service
    mkdir -p /run/sysota
    cat <<SYSOTAD_CONF >/run/sysota/sysotad.conf
    [Device]
    Maker = Mr Potato
    Model = DevKit 9000

    [Update]
    ServerURL = https://updates.example.org/
    SystemImagePackage = potato-os
    Stream = latest/stable
    SYSOTAD_CONF
execute: |
    # The three properties can be read
    test "$(busctl get-property org.oniroproject.sysota1 /org/oniroproject/sysota1/Service org.oniroproject.sysota1.Service Maker)" = 's "Mr Potato"'
    test "$(busctl get-property org.oniroproject.sysota1 /org/oniroproject/sysota1/Service org.oniroproject.sysota1.Service Model)" = 's "DevKit 9000"'
    test "$(busctl get-property org.oniroproject.sysota1 /org/oniroproject/sysota1/Service org.oniroproject.sysota1.Service ServerURL)" = 's "https://updates.example.org/"'
    test "$(busctl get-property org.oniroproject.sysota1 /org/oniroproject/sysota1/Service org.oniroproject.sysota1.Service SystemImagePackage)" = 's "potato-os"'
    test "$(busctl get-property org.oniroproject.sysota1 /org/oniroproject/sysota1/Service org.oniroproject.sysota1.Service Stream)" = 's "latest/stable"'

    # The 'ServerURL' property can be written as well
    test "$(busctl set-property org.oniroproject.sysota1 /org/oniroproject/sysota1/Service org.oniroproject.sysota1.Service ServerURL s https://netota.example.org/)" = ''
    test "$(busctl get-property org.oniroproject.sysota1 /org/oniroproject/sysota1/Service org.oniroproject.sysota1.Service ServerURL)" = 's "https://netota.example.org/"'

    # The 'SystemImagePackage' property can be modified.
    test "$(busctl set-property org.oniroproject.sysota1 /org/oniroproject/sysota1/Service org.oniroproject.sysota1.Service SystemImagePackage s french-fries-os)" = ''
    test "$(busctl get-property org.oniroproject.sysota1 /org/oniroproject/sysota1/Service org.oniroproject.sysota1.Service SystemImagePackage)" = 's "french-fries-os"'

    # The 'Stream' property can be modified.
    test "$(busctl set-property org.oniroproject.sysota1 /org/oniroproject/sysota1/Service org.oniroproject.sysota1.Service Stream s latest/nightly)" = ''
    test "$(busctl get-property org.oniroproject.sysota1 /org/oniroproject/sysota1/Service org.oniroproject.sysota1.Service Stream)" = 's "latest/nightly"'

    # This needs a netota repository to work.
    if false; then
        # TODO: this part needs to be split to a separate test as it's non-trivial now.
        # The GetStreams method can be called
        test "$(busctl call org.oniroproject.sysota1 /org/oniroproject/sysota1/Service org.oniroproject.sysota1.Service GetStreams)" = ''
        journalctl -u sysotad.service | MATCH 'called GetStreams'

        # TODO: this part needs to be split to a separate test as it's non-trivial now.
        # The Update method can be called with arbitrary hints
        test "$(busctl call org.oniroproject.sysota1 /org/oniroproject/sysota1/Service org.oniroproject.sysota1.Service Update a{ss} 1 hint value)" = 'o "/org/oniroproject/sysota1/Service/Operation/1"'
        journalctl -u sysotad.service | MATCH 'called Update with hints map\[hint:value\]'

        # The resulting operation doesn't do anything yet but has several properties.
        test "$(busctl get-property org.oniroproject.sysota1 /org/oniroproject/sysota1/Service/Operation/1 org.oniroproject.sysota1.Operation Kind)" = 's "unknown"'
        test "$(busctl get-property org.oniroproject.sysota1 /org/oniroproject/sysota1/Service/Operation/1 org.oniroproject.sysota1.Operation State)" = 'i 1'
        test "$(busctl get-property org.oniroproject.sysota1 /org/oniroproject/sysota1/Service/Operation/1 org.oniroproject.sysota1.Operation Progress)" = 'd 0'
        test "$(busctl get-property org.oniroproject.sysota1 /org/oniroproject/sysota1/Service/Operation/1 org.oniroproject.sysota1.Operation Error)" = 's ""'
    fi
restore: |
    rm -f /run/sysota/sysotad.conf
    systemctl restart sysotad.service
