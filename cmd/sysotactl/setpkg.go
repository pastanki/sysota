// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

package sysotactl

import (
	"context"
	"errors"
	"flag"
	"fmt"
	"io"

	"gitlab.com/zygoon/go-cmdr"

	"gitlab.com/zygoon/sysota/pkg/errutil"
)

// SetPackageCmd is the "sysotactl set-package" sub-command.
type SetPackageCmd struct{}

// OneLiner returns a help message when used inside Router.
func (SetPackageCmd) OneLiner() string {
	return "Set the name of the system image package"
}

func (SetPackageCmd) parseArgs(stderr io.Writer, args []string) (packageName string, err error) {
	fs := flag.NewFlagSet(Name+" set-package", flag.ContinueOnError)
	fs.SetOutput(stderr)

	fs.Usage = func() {
		_, _ = fmt.Fprintf(fs.Output(), "Usage: %s set-package NAME\n", Name)
	}

	if err := fs.Parse(args); err != nil {
		if errors.Is(err, flag.ErrHelp) {
			w := fs.Output()

			_, _ = fmt.Fprintf(w, "\n")
			_, _ = fmt.Fprintf(w, "The set-package command changes the package used for OS images.\n\n")
			_, _ = fmt.Fprintf(w, "Updates are only installed if the machine is compatible.\n")
		}

		return "", err
	}

	if fs.NArg() != 1 {
		fs.Usage()

		return "", flag.ErrHelp
	}

	return fs.Arg(0), nil
}

// Run calls properties.Set with the appropriate property name.
func (cmd SetPackageCmd) Run(ctx context.Context, args []string) error {
	_, stdout, stderr := cmdr.Stdio(ctx)

	packageName, err := cmd.parseArgs(stderr, args)
	if err != nil {
		return err
	}

	client, err := NewClient()
	if err != nil {
		return err
	}

	defer func() {
		errutil.Forward(&err, client.Close())
	}()

	_, _ = fmt.Fprintf(stdout, "Setting system image package name to %q\n", packageName)

	if err := client.SetSystemPackage(ctx, packageName); err != nil {
		return fmt.Errorf("cannot set system image package name to %s: %w", packageName, err)
	}

	return nil
}
