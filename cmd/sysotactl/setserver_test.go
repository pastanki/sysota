// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

package sysotactl_test

import (
	"context"
	"errors"
	"flag"

	"github.com/godbus/dbus/v5"
	"github.com/godbus/dbus/v5/prop"
	. "gopkg.in/check.v1"

	"gitlab.com/zygoon/sysota/pkg/testutil"
)

type setServerSuite struct {
	cmdSuite
}

var _ = Suite(&setServerSuite{})

func (s *setServerSuite) TestHelp(c *C) {
	ctx, stdout, stderr := testutil.BufferOutput(context.TODO())
	err := s.cmd.Run(ctx, []string{"set-server", "--help"})

	c.Assert(err, Equals, flag.ErrHelp)
	c.Check(stdout.String(), Equals, "")
	c.Check(stderr.String(), Equals, ""+
		"Usage: sysotactl set-server URL\n"+
		"\n"+
		"The set-server command changes the URL of the NetOTA server to use.\n")
}

func (s *setServerSuite) TestMissingArgument(c *C) {
	ctx, stdout, stderr := testutil.BufferOutput(context.TODO())
	err := s.cmd.Run(ctx, []string{"set-server"})

	c.Assert(err, Equals, flag.ErrHelp)
	c.Check(stdout.String(), Equals, "")
	c.Check(stderr.String(), Equals, "Usage: sysotactl set-server URL\n")
}

func (s *setServerSuite) TestSuccessfulChange(c *C) {
	ctx, stdout, stderr := testutil.BufferOutput(context.TODO())
	err := s.cmd.Run(ctx, []string{"set-server", "http://localhost/"})

	c.Assert(err, IsNil)
	c.Check(stdout.String(), Equals, "Setting update server to \"http://localhost/\"\n")
	c.Check(stderr.String(), Equals, "")

	// XXX: why is this returning a pointer to a string?
	c.Check(*s.props.GetMust("ServerURL").(*string), Equals, "http://localhost/")
}

func (s *setServerSuite) TestClientSideValidationFailure(c *C) {
	ctx, stdout, stderr := testutil.BufferOutput(context.TODO())
	err := s.cmd.Run(ctx, []string{"set-server", "://potato/place"})

	c.Assert(err, ErrorMatches, "cannot parse server URL: parse \"://potato/place\": missing protocol scheme")
	c.Check(stdout.String(), Equals, "")
	c.Check(stderr.String(), Equals, "")

	c.Check(s.props.GetMust("ServerURL").(string), Equals, "https://example.org/")
}

func (s *setServerSuite) TestServerSideValidationFailure(c *C) {
	s.MockChangeFunc(func(ch *prop.Change) *dbus.Error {
		return dbus.MakeFailedError(errors.New("boom"))
	})

	ctx, stdout, stderr := testutil.BufferOutput(context.TODO())
	err := s.cmd.Run(ctx, []string{"set-server", "http://example.net/"})

	c.Assert(err, ErrorMatches, "cannot set update server URL to http://example.net/: boom")
	c.Check(stdout.String(), Equals, "Setting update server to \"http://example.net/\"\n")
	c.Check(stderr.String(), Equals, "")

	c.Check(s.props.GetMust("ServerURL").(string), Equals, "https://example.org/")
}
