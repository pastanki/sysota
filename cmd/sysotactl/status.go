// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

package sysotactl

import (
	"context"
	"errors"
	"flag"
	"fmt"
	"io"

	"gitlab.com/zygoon/go-cmdr"

	"gitlab.com/zygoon/sysota/pkg/errutil"
)

// StatusCmd is the "sysotactl status" sub-command.
type StatusCmd struct{}

// OneLiner returns a help message when used inside Router.
func (StatusCmd) OneLiner() string {
	return "Display current state and configuration"
}

func (StatusCmd) parseArgs(stderr io.Writer, args []string) error {
	fs := flag.NewFlagSet(Name+" status", flag.ContinueOnError)
	fs.SetOutput(stderr)

	fs.Usage = func() {
		_, _ = fmt.Fprintf(fs.Output(), "Usage: %s status\n", Name)
	}

	if err := fs.Parse(args); err != nil {
		if errors.Is(err, flag.ErrHelp) {
			w := fs.Output()

			_, _ = fmt.Fprintf(w, "\n")
			_, _ = fmt.Fprintf(w, "The status command displays configuration of the system.\n")
		}

		return err
	}

	if fs.NArg() != 0 {
		fs.Usage()

		return flag.ErrHelp
	}

	return nil
}

// Run calls properties.GetAll and renders the response as a table.
func (cmd StatusCmd) Run(ctx context.Context, args []string) error {
	_, stdout, stderr := cmdr.Stdio(ctx)

	if err := cmd.parseArgs(stderr, args); err != nil {
		return err
	}

	client, err := NewClient()
	if err != nil {
		return err
	}

	defer func() {
		errutil.Forward(&err, client.Close())
	}()

	p, err := client.Properties(ctx)
	if err != nil {
		return fmt.Errorf("cannot retrieve system status: %w", err)
	}

	_, _ = fmt.Fprintf(stdout, "** Device **\n\n")

	_, _ = fmt.Fprintf(stdout, "  Maker: %s\n", p.Maker)
	_, _ = fmt.Fprintf(stdout, "  Model: %s\n", p.Model)

	if p.InstalledPackageName != "" {
		_, _ = fmt.Fprintf(stdout, "\n** Installed System Image **\n\n")

		_, _ = fmt.Fprintf(stdout, "   Package: %s\n", p.InstalledPackageName)
		_, _ = fmt.Fprintf(stdout, "   Version: %s\n", p.InstalledPackageVersion)
		_, _ = fmt.Fprintf(stdout, "  Revision: %s\n", p.InstalledSourceRevision)
	}

	_, _ = fmt.Fprintf(stdout, "\n** Update Server **\n\n")

	_, _ = fmt.Fprintf(stdout, "  Server URL: %s\n", p.ServerURL)
	_, _ = fmt.Fprintf(stdout, "     Package: %s\n", p.SystemImagePackage)
	_, _ = fmt.Fprintf(stdout, "      Stream: %s\n", p.Stream)

	return nil
}
