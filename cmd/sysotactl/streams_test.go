// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

package sysotactl_test

import (
	"context"
	"errors"
	"flag"
	"time"

	"github.com/godbus/dbus/v5"
	"gitlab.com/zygoon/netota"
	. "gopkg.in/check.v1"

	"gitlab.com/zygoon/sysota/pkg/testutil"
)

type streamsSuite struct {
	cmdSuite
}

var _ = Suite(&streamsSuite{})

func (s *streamsSuite) TestHelp(c *C) {
	ctx, stdout, stderr := testutil.BufferOutput(context.TODO())
	err := s.cmd.Run(ctx, []string{"streams", "--help"})

	c.Assert(err, Equals, flag.ErrHelp)
	c.Check(stdout.String(), Equals, "")
	c.Check(stderr.String(), Equals, ""+
		"Usage: sysotactl streams\n"+
		"\n"+
		"The streams command displays available update streams\n")
}

func (s *streamsSuite) TestExcessiveArgument(c *C) {
	ctx, stdout, stderr := testutil.BufferOutput(context.TODO())
	err := s.cmd.Run(ctx, []string{"streams", "potato"})

	c.Assert(err, Equals, flag.ErrHelp)
	c.Check(stdout.String(), Equals, "")
	c.Check(stderr.String(), Equals, "Usage: sysotactl streams\n")
}

func (s *streamsSuite) TestSuccess(c *C) {
	s.MockStreamsFunc(func() (map[string]netota.Stream, *dbus.Error) {
		streams := map[string]netota.Stream{
			"2.x/stable": {
				Name:           "2.x/stable",
				PackageVersion: "2.0",
				SourceRevision: "9297675aaeb13d95dd6619fb48e2600ea16e9c26",
				SourceAliases:  []string{"v2.0.0"},
				UpdatedOnSec:   time.Date(2021, time.December, 29, 19, 31, 5, 0, time.UTC).Unix(),
			},
			"1.x/stable": {
				Name:       "1.x/stable",
				IsClosed:   true,
				ReplacedBy: "2.x/stable",
			},
		}

		return streams, nil
	})

	ctx, stdout, stderr := testutil.BufferOutput(context.TODO())
	err := s.cmd.Run(ctx, []string{"streams"})

	c.Assert(err, IsNil)
	c.Check(stdout.String(), Equals, ""+
		"1.x/stable:\n"+
		"    closed: true\n"+
		"    replaced-by: 2.x/stable\n"+
		"2.x/stable:\n"+
		"    updated-on: 2021-12-29T19:31:05Z\n"+
		"    package-version: \"2.0\"\n"+
		"    source-revision: 9297675aaeb13d95dd6619fb48e2600ea16e9c26\n"+
		"    source-aliases: [v2.0.0]\n")
	c.Check(stderr.String(), Equals, "")
}

func (s *streamsSuite) TestFailure(c *C) {
	s.MockStreamsFunc(func() (map[string]netota.Stream, *dbus.Error) {
		return nil, dbus.MakeFailedError(errors.New("boom"))
	})

	ctx, stdout, stderr := testutil.BufferOutput(context.TODO())
	err := s.cmd.Run(ctx, []string{"streams"})

	c.Assert(err, ErrorMatches, "boom")
	c.Check(stdout.String(), Equals, "")
	c.Check(stderr.String(), Equals, "")
}
