// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

package sysotactl_test

import (
	"bufio"
	"bytes"
	"context"
	"errors"
	"flag"
	"io"
	"os"
	"sync"
	"time"

	"github.com/godbus/dbus/v5"
	"gitlab.com/zygoon/go-cmdr"
	. "gopkg.in/check.v1"

	"gitlab.com/zygoon/sysota/cmd/sysotad/updatehosted/oper"
	opspec "gitlab.com/zygoon/sysota/cmd/sysotad/updatehosted/oper/spec"
	"gitlab.com/zygoon/sysota/pkg/testutil"
)

type updateSuite struct {
	cmdSuite
}

var _ = Suite(&updateSuite{})

func (s *updateSuite) TestHelp(c *C) {
	ctx, stdout, stderr := testutil.BufferOutput(context.TODO())
	err := s.cmd.Run(ctx, []string{"update", "--help"})

	c.Assert(err, Equals, flag.ErrHelp)
	c.Check(stdout.String(), Equals, "")
	c.Check(stderr.String(), Equals, ""+
		"Usage: sysotactl update\n"+
		"\n"+
		"The update command checks and applies update to the system image.\n"+
		"The device is rebooted automatically during the update process.\n")
}

func (s *updateSuite) TestExcessiveArgument(c *C) {
	ctx, stdout, stderr := testutil.BufferOutput(context.TODO())
	err := s.cmd.Run(ctx, []string{"update", "potato"})

	c.Assert(err, Equals, flag.ErrHelp)
	c.Check(stdout.String(), Equals, "")
	c.Check(stderr.String(), Equals, "Usage: sysotactl update\n")
}

func (s *updateSuite) TestNoUpdates(c *C) {
	ctx, stdout, stderr := testutil.BufferOutput(context.TODO())
	err := s.cmd.Run(ctx, []string{"update"})

	c.Assert(err, ErrorMatches, "cannot initialize system update: system is up-to-date")
	c.Check(stdout.String(), Equals, "")
	c.Check(stderr.String(), Equals, "")
}

func (s *updateSuite) TestUpdateSuccessNoSleep(c *C) {
	s.testUpdateSuccess(c, 0)
}

func (s *updateSuite) TestUpdateSuccessNanoSleep(c *C) {
	s.testUpdateSuccess(c, time.Nanosecond)
}

func (s *updateSuite) TestUpdateSuccessMicroSleep(c *C) {
	s.testUpdateSuccess(c, time.Microsecond)
}

func (s *updateSuite) TestUpdateSuccessMilliSleep(c *C) {
	s.testUpdateSuccess(c, time.Millisecond)
}

func (s *updateSuite) testUpdateSuccess(c *C, d time.Duration) {
	s.MockUpdateFn(func(hints map[string]dbus.Variant) (dbus.ObjectPath, *dbus.Error) {
		op, err := oper.NewOperation(s.conn, 1)
		if err != nil {
			return "", dbus.MakeFailedError(err)
		}

		op.Go(opspec.SystemUpdate, func() error {
			for prog := 0; prog <= 100; prog += 10 {
				op.SetProgress(opspec.Progress(prog))
				time.Sleep(d)
			}
			return nil
		})

		return op.ObjectPath(), nil
	})

	ctx, stdout, stderr := testutil.BufferOutput(context.TODO())
	err := s.cmd.Run(ctx, []string{"update"})

	c.Assert(err, IsNil)

	// The exact messages printed vary on the speed of the reader and writer
	// but at the end, we must see that the update has finished.
	found := false
	stdoutCopy := stdout.String()

	sc := bufio.NewScanner(stdout)
	for sc.Scan() {
		if sc.Text() == "kind:system-update, state:done, progress:100%" {
			found = true
		}
	}

	c.Assert(found, Equals, true, Commentf("stdout: %s", stdoutCopy))
	c.Check(stderr.String(), Equals, "")
}

func (s *updateSuite) TestUpdateErrorNoSleep(c *C) {
	s.testUpdateError(c, 0)
}

func (s *updateSuite) TestUpdateErrorNanoSleep(c *C) {
	s.testUpdateError(c, time.Nanosecond)
}

func (s *updateSuite) TestUpdateErrorMicroSleep(c *C) {
	s.testUpdateError(c, time.Microsecond)
}

func (s *updateSuite) TestUpdateErrorMilliSleep(c *C) {
	s.testUpdateError(c, time.Millisecond)
}

func (s *updateSuite) testUpdateError(c *C, d time.Duration) {
	s.MockUpdateFn(func(hints map[string]dbus.Variant) (dbus.ObjectPath, *dbus.Error) {
		op, err := oper.NewOperation(s.conn, 1)
		if err != nil {
			return "", dbus.MakeFailedError(err)
		}

		op.Go(opspec.SystemUpdate, func() error {
			for prog := 0; prog <= 50; prog += 10 {
				op.SetProgress(opspec.Progress(prog))
				time.Sleep(d)
			}
			return errors.New("cannot download update")
		})

		return op.ObjectPath(), nil
	})

	ctx, stdout, stderr := testutil.BufferOutput(context.TODO())
	err := s.cmd.Run(ctx, []string{"update"})

	c.Assert(err, ErrorMatches, "cannot download update")

	// The exact messages printed vary on the speed of the reader and writer
	// but at the end, we must see that the update has finished.
	found := false
	stdoutCopy := stdout.String()

	sc := bufio.NewScanner(stdout)
	for sc.Scan() {
		if sc.Text() == "kind:system-update, state:done, progress:50%" {
			found = true
		}
	}

	c.Assert(found, Equals, true, Commentf("stdout: %s", stdoutCopy))
	c.Check(stderr.String(), Equals, "")
}

func (s *updateSuite) TestUpdateCancelled(c *C) {
	ctx, cancel := context.WithCancel(context.Background())

	s.MockUpdateFn(func(hints map[string]dbus.Variant) (dbus.ObjectPath, *dbus.Error) {
		op, err := oper.NewOperation(s.conn, 1)
		if err != nil {
			return "", dbus.MakeFailedError(err)
		}

		// Pretend that the update is cancelled as soon as it is created.
		cancel()

		return op.ObjectPath(), nil
	})

	ctx, _, stderr := testutil.BufferOutput(ctx)
	err := s.cmd.Run(ctx, []string{"update"})

	c.Assert(err, ErrorMatches, "cannot initialize system update: context canceled")

	// The precise content of stdout varies.
	c.Check(stderr.String(), Equals, "")
}

func pipedOutput(ctx context.Context) (ctx2 context.Context, stdout *io.PipeReader, stderr *bytes.Buffer) {
	stdoutReader, stdoutWriter := io.Pipe()
	stderr = new(bytes.Buffer)

	return cmdr.WithStdio(ctx, os.Stdin, stdoutWriter, stderr), stdoutReader, stderr
}

func (s *updateSuite) TestUpdateCancelledLater(c *C) {
	ctx, cancel := context.WithCancel(context.Background())

	// Wait group for the go-routines from Operation.Go and the test observer.
	var wg sync.WaitGroup

	wg.Add(2)

	// Channel used for stalling the update operation after it had sent one
	// update but before it had completed.
	stallCh := make(chan struct{})
	defer close(stallCh)

	// Pipe used to observe stdout as printed by "sysotactl update".
	ctx, stdout, stderr := pipedOutput(ctx)

	s.MockUpdateFn(func(hints map[string]dbus.Variant) (dbus.ObjectPath, *dbus.Error) {
		op, err := oper.NewOperation(s.conn, 1)
		if err != nil {
			return "", dbus.MakeFailedError(err)
		}

		op.Go(opspec.SystemUpdate, func() error {
			op.SetProgress(50)
			<-stallCh

			wg.Done()

			return nil
		})

		return op.ObjectPath(), nil
	})

	scan := bufio.NewScanner(stdout)

	go func() {
		// Make sure we see the output with the expected progress. We are not
		// synchronized with the "sysotactl update" go-routine in any other way.
		//
		// This gives us a guarantee that the operation has started and the
		// changes were processed and displayed.
		scan.Scan()
		c.Check(scan.Text(), Equals, "kind:system-update, state:running, progress:50%")

		// Cancel the context after seeing one line of output from the command.
		cancel()

		c.Assert(scan.Err(), IsNil)
		c.Assert(stdout.Close(), IsNil)

		wg.Done()
	}()

	err := s.cmd.Run(ctx, []string{"update"})
	c.Assert(err, ErrorMatches, "cannot complete system update: context canceled")

	// Allow the operation to complete.
	stallCh <- struct{}{}

	// Wait for all go-routines to finish.
	wg.Wait()

	// The precise content of stdout varies.
	c.Check(stderr.String(), Equals, "")
}
