// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

// Package raucadapter contains SystemOTA boot.Protocol adapter for RAUC.
//
// The adapter is meant to be hosted by raucinterface.Service.
package raucadapter

import (
	"errors"

	"gitlab.com/zygoon/sysota/boot"
	"gitlab.com/zygoon/sysota/rauc/installhandler"
)

// ErrProtocolViolation records unexpected usage of RAUC custom boot backend handler.
//
// This can happen if the logic in SystemOTA is flawed or if RAUC changes
// radically, extending the protocol in a way that requires updates to
// SystemOTA.
var ErrProtocolViolation = errors.New("violation of protocol between SystemOTA and RAUC")

// Adapter uses SystemOTA boot.Protocol and ota.SystemState to implement boothandler.Handler.
type Adapter struct {
	proto          boot.Protocol
	bootModeHolder BootModeHolder
}

// BootModeHolder is an interface exposing boot mode control.
//
// This interface exits, mainly, so that modifications of BootMode can be
// broadcast over D-Bus.
type BootModeHolder interface {
	BootMode() boot.Mode
	SetBootMode(m boot.Mode, isRollback bool) error
}

// New returns a new Adapter implemented wrapping given boot.Protocol.
//
// RAUC and SystemOTA differ in how the boot loader is handled. RAUC uses a
// stateful protocol, where each slot has a persistent state and there is a
// persistent primary slot used for booting. SystemOTA uses a transactional
// interface and has only one element of state that is not captured in the boot
// loader itself, namely boot.Mode, where the system may be expected to normally
// or in the one-off try mode.
func New(proto boot.Protocol, bootModeHolder BootModeHolder) *Adapter {
	return &Adapter{proto: proto, bootModeHolder: bootModeHolder}
}

// PrimarySlot returns the name of the RAUC primary boot slot.
//
// SystemOTA does not model the primary slot directly. The system boot.Mode
// state is used to pick either active slot (in normal mode) or inactive slot
// (in try mode).
func (adapter *Adapter) PrimarySlot() (boot.Slot, error) {
	switch adapter.bootModeHolder.BootMode() {
	case boot.Normal:
		return adapter.queryActiveOrPristine()
	case boot.Try:
		// TODO(zyga): consider using boot.SynthesizeInactiveSlot and dropping
		// QueryInactive from the API.
		return adapter.queryInactiveOrPristine()
	default:
		return boot.InvalidSlot, boot.ErrInvalidBootMode
	}
}

// SetPrimarySlot sets the name of the RAUC primary boot slot.
//
// SystemOTA does not model the primary slot directly. When in normal boot mode,
// attempt to set the inactive boot slot as primary commences an update
// transaction. This transaction is finished with a call to SetSlotState, which
// either commits or rolls back the transaction.
//
// In case of invalid input the error is boot.ErrInvalidSlot, Other errors are
// those from the concrete implementation of boot.Protocol or
// ErrProtocolViolation.
func (adapter *Adapter) SetPrimarySlot(slot boot.Slot) error {
	if slot != boot.SlotA && slot != boot.SlotB {
		return boot.ErrInvalidSlot
	}

	activeSlot, err := adapter.queryActiveOrPristine()
	if err != nil {
		return err
	}

	switch adapter.bootModeHolder.BootMode() {
	case boot.Normal:
		if slot == activeSlot {
			// No-op, in normal mode, the active slot is always primary.
			return nil
		}

		// We are in normal boot mode and RAUC is attempting to set the
		// primary slot to the inactive slot. This begins the update
		// transaction. Ask the bootloader to try to switch to the given
		// (inactive) slot and if successful, switch to try-boot mode.
		if err := adapter.proto.TrySwitch(slot); err != nil {
			return err
		}

		return adapter.bootModeHolder.SetBootMode(boot.Try, false)
	case boot.Try:
		if slot == activeSlot {
			// Protocol violation, this does not happen during "rauc install".
			//
			// XXX(zyga): this could also be handled as a transaction cancel
			// operation, by calling adapter.Proto.CancelSwitch() and setting
			// boot mode back to normal.
			return ErrProtocolViolation
		}

		// No-op, in try mode, the inactive slot is always primary.
		return nil
	default:
		return boot.ErrInvalidBootMode
	}
}

// SlotState returns the good/bad state of a given slot.
//
// SystemOTA does not model the state of each slot directly. It is assumed that the
// active slot is always good and that the inactive slot is always bad. Note
// that boot.Protocol.TrySwitch does not modify the active slot, so the system
// does attempt to boot into a known "bad" slot, this is intentional and matches
// what RAUC expects.
func (adapter *Adapter) SlotState(slot boot.Slot) (boot.SlotState, error) {
	if slot != boot.SlotA && slot != boot.SlotB {
		return boot.InvalidSlotState, boot.ErrInvalidSlot
	}

	active, err := adapter.queryActiveOrPristine()
	if err != nil {
		return boot.InvalidSlotState, err
	}

	if slot == active {
		return boot.GoodSlot, nil
	}

	return boot.BadSlot, nil
}

// SetSlotState sets the good/bad state of a given slot.
//
// SystemOTA does not model the state of each slot directly. The only state is
// boot.Mode - either normal boot or try-boot and whatever the bootloader
// considers to be the active slot.
//
// SetSlotState is expected to be called to initiate and finalize a bundle
// install operation, or in other words a system update.
//
// When SetSlotState is called to initialize the update operation, the inactive
// slot is marked as bad and then set as the primary boot slot. SystemOTA
// recognizes this, verifies that the state is set to bad and does not do
// anything more.
//
// When SetSlotState is called called to finalize a successful update operation
// the state of the primary slot is changed to good. Note that at this stage
// boot.Mode is Try and that the primary slot is still the inactive slot.
//
// When SetSlotState is called to finalize a failed or aborted update operation
// the only difference from the previous situation is that the state is bad
// instead of good.
//
// In case of invalid input the error is boot.ErrInvalidSlot,
// boot.ErrInvalidSlotState. Other errors are those from the concrete
// implementation of boot.Protocol or ErrProtocolViolation.
func (adapter *Adapter) SetSlotState(slot boot.Slot, state boot.SlotState) error {
	if slot != boot.SlotA && slot != boot.SlotB {
		return boot.ErrInvalidSlot
	}

	if state != boot.GoodSlot && state != boot.BadSlot {
		return boot.ErrInvalidSlotState
	}

	active, err := adapter.queryActiveOrPristine()
	if err != nil {
		return err
	}

	inactive := boot.SynthesizeInactiveSlot(active)
	bootMode := adapter.bootModeHolder.BootMode()

	// Switch by boot mode, then switch by slot state and finally look at the
	// slot we've got. This exact same logic can be re-written in several
	// different styles but I've found this to be easiest to grasp while
	// retaining consistency.
	switch bootMode {
	case boot.Normal:
		switch state {
		case boot.GoodSlot:
			if slot == active {
				// Affirmation of an expected, synthesized state.
				//
				// SystemOTA tells RAUC that the active slot is always good.
				// so this doesn't change anything.
				return nil
			}
		case boot.BadSlot:
			if slot == inactive {
				// Affirmation of an expected, synthesized state.
				//
				// SystemOTA tells RAUC that the inactive slot is always bad
				// so this doesn't change anything.
				return nil
			}
		}
	case boot.Try:
		switch state {
		case boot.GoodSlot:
			if slot == active {
				// Affirmation of an expected, synthesized state.
				//
				// SystemOTA always tells RAUC that the active slot is good
				// (even in try mode). For clarity, in try mode the SystemOTA
				// active slot is not the RAUC primary slot.
				return nil
			}

			// Note that due to the check above, the slot must therefore be inactive.

			// Commit of an update transaction.
			if err := adapter.proto.CommitSwitch(); err != nil {
				return err
			}

			return adapter.bootModeHolder.SetBootMode(boot.Normal, false)
		case boot.BadSlot:
			if slot == inactive {
				// Rollback of an update transaction.
				// Note that we set boot mode even if CancelSwitch failed because by
				// this time, we have rolled back already.
				if err := adapter.bootModeHolder.SetBootMode(boot.Normal, true); err != nil {
					return err
				}

				if err := adapter.proto.CancelSwitch(); err != nil {
					return err
				}

				return nil
			}
		}
	}
	// Everything else is unexpected.
	return ErrProtocolViolation
}

// PreInstall forwards the call to compatible boot.Protocol implementations.
func (adapter *Adapter) PreInstall(env *installhandler.Environment) error {
	type preInstallAware interface {
		PreInstall(env *installhandler.Environment) error
	}

	if v, ok := adapter.proto.(preInstallAware); ok {
		return v.PreInstall(env)
	}

	return nil
}

// PostInstall forwards the call to compatible boot.Protocol implementations.
func (adapter *Adapter) PostInstall(env *installhandler.Environment) error {
	type postInstallAware interface {
		PostInstall(env *installhandler.Environment) error
	}

	if v, ok := adapter.proto.(postInstallAware); ok {
		return v.PostInstall(env)
	}

	return nil
}

func (adapter *Adapter) queryActiveOrPristine() (boot.Slot, error) {
	active, err := adapter.proto.QueryActive()
	if errors.Is(err, boot.ErrPristineBootConfig) {
		active = boot.SlotA
		err = nil
	}

	return active, err
}

func (adapter *Adapter) queryInactiveOrPristine() (boot.Slot, error) {
	active, err := adapter.proto.QueryInactive()
	if errors.Is(err, boot.ErrPristineBootConfig) {
		active = boot.SlotB
		err = nil
	}

	return active, err
}
