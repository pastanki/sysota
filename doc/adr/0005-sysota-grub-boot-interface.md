<!--
SPDX-License-Identifier: CC-BY-4.0
SPDX-FileCopyrightText: Huawei Inc.
-->
# 5. SysOTA GRUB Boot Interface

Date: 2022-03-17

## Status

Accepted

## Context

During integration with RAUC and GRUB we have discovered problems with using
existing RAUC integration scripts based on ORDER, A_OK, A_TRY, B_OK and B_TRY.
RAUC's GRUB handler unconditionally sets, in a scenario where the system updates
from A to B, the variable B_OK=1 immediately after programming the slot and
swaps ORDER so that the newly written slot is booted first.

RAUC and SysOTA bootloader interfaces differ somewhat, as RAUC models any number
of slots and their state, while SysOTA models only the active slot and has a
transactional try-boot that can pick the other slot for a one-time boot. The
transaction is automatically rolled back unless committed on a successful boot
to the new slot.

## Decision

Having spent enough time trying to make sense of the examples in
meta-rauc-community I've decided it's just easier to implement something that is
closer to SysOTA's model.

SysOTA contains bootloader integration logic for GRUB and does not rely on RAUCs
boot loader logic, even though such logic already exists. This makes it easier
to offer transactional updates without having to wrap one's head around the
exact desired semantics of the per-slot OK and TRY variables.

## Consequences

SysOTA needs to carry additional custom code but the way the SysOTA boot
protocol concept is implemented for GRUB is much easier to understand.
