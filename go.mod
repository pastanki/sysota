module gitlab.com/zygoon/sysota

go 1.16

require (
	github.com/godbus/dbus/v5 v5.0.6
	gitlab.com/zygoon/go-cmdr v1.2.0
	gitlab.com/zygoon/go-grub v0.1.0
	gitlab.com/zygoon/go-ini v1.0.2
	gitlab.com/zygoon/go-raspi v1.1.0
	gitlab.com/zygoon/go-squashfstools v1.0.0
	gitlab.com/zygoon/netota v0.3.1
	gopkg.in/check.v1 v1.0.0-20201130134442-10cb98267c6c
	gopkg.in/yaml.v3 v3.0.0-20210107192922-496545a6307b
)
