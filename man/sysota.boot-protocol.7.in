.\" SPDX-License-Identifier: Apache-2.0
.\" SPDX-FileCopyrightText: Huawei Inc.
.Dd Sep 14, 2021
.Os @PROJECT_NAME@ @PROJECT_VERSION@
.Dt sysota.boot-protocol 7 PRM
.Sh NAME
.Nm sysota.boot-protocol
.Nd SystemOTA protocol for interacting with boot loaders
.Sh SYNOPSIS
.Pp
QueryActive() (Slot, error)
.Pp
QueryInactive() (Slot, error)
.Pp
TrySwitch(Slot) error
.Pp
Reboot(RebootFlags) error
.Pp
CommitSwitch() error
.Pp
CancelSwitch() error
.Sh DESCRIPTION
SystemOTA provides an abstraction layer for interacting with boot loaders as a
Go interface comprised of a set of functions discussed below. The interface
assumes an
.Em A
-
.Em B
partitioning system, where two complete boot configurations are possible.
During
.Em regular boot
the boot loader picks the
.Em active configuration
for booting. In practice it is a set of compatible
.Em kernel ,
optional
.Em initrd
and
.Em device tree ,
together with a
.Em root file system ,
either read-only or writable. If the root file system is read only then the set
also includes a matching
.Em mutable state ,
modeled as a dedicated partition or as a dedicated directory in a shared
partition.
.Pp
Using local state accessible to the boot loader, the boot process may be
diverted to the second configuration for a one-off
.Em try boot ,
with matching set of elements enumerated earlier.
.Pp
This abstraction should map to commonly used boot loaders that can either
implement local boot counting scheme and scripted logic and access to a small
portion of using non-volatile memory, or by performing
.Em platform-specific reboot
to request the platform boot loader to use the
.Em inactive configuration
for booting.
.Sh CALL DIAGRAM
The following diagram illustrates how the methods of the boot protocol are
called during the update process. The diagram does not show interactions
between SystemOTA and the OS state management handler.
.Bd -literal
              system is running from
                active configuration
                       |
                       v
                  \fITrySwitch()\fP
                       |
       inactive configuration is created
                       |
                       v
             \fIReboot(RebootTryBoot)\fP
               |               |
    system is running from     x   system fails to boot and gets
    inactive configuration     .   power-cycled, e.g. by a watchdog
               |               .   or manually by a person
               |               ..........................
               v                                        .
        health assessment -----------> NOT HEALTHY      .
               |                            |           .
            HEALTHY                         v           .
               |                        \fIReboot(0)\fP       .
               |                            |           .
               |                            |<...........
               |                            |
               |                  system is running from
               |                   active configuration
               |                            |
               v                            v
         \fICommitSwitch()\fP               \fICancelSwitch()\fP
               |                            |
       inactive configuration     inactive configuration
    replaces active configuration       is removed
               |                            |
               v                            v
       UPDATE SUCCESSFUL              UPDATE FAILED
.Ed
.Sh METHODS
The following sub-sections discuss the semantics expected from each of the
boot protocol methods.
.Ss QueryActive
.Nm QueryActive
reads the persistent boot configuration and returns the
.Em slot
used during regular boot process. The return value is always either
.Em A
or
.Em B .
.Ss QueryInactive
.Nm QueryInactive
returns the slot not returned by
.Nm QueryActive .
This function may be removed from future versions of the boot protocol.
.Ss TrySwitch
.Nm TrySwitch
performs platform-specific operations to prepare the
.Em inactive configuration
to boot to the given slot. The operation may have persistent side-effects for
as long as they do not affect the
.Em regular boot .
.Ss Reboot
.Nm Reboot
must be called with either
.Em 0
to request a standard system reboot, followed by
.Em regular boot
into the
.Em active configuration ,
or with
.Nm RebootTryBoot
to request a standard system reboot, followed by
.Em try boot
into the
.Em inactive configuration .
.Ss CommitSwitch
.Nm CommitSwitch replaces the
.Em active configuration
with the contents of the
.Em inactive configuration .
.Ss CancelSwitch
.Nm CancelSwitch
removes any persistent modifications, if any, performed by
.Nm TrySwitch .
