// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

package ota

import (
	"errors"
	"fmt"
	"os"
	"path/filepath"
	"sync"

	"gitlab.com/zygoon/go-ini"

	"gitlab.com/zygoon/sysota/boot"
	"gitlab.com/zygoon/sysota/pkg/atomicfile"
	"gitlab.com/zygoon/sysota/pkg/errutil"
)

// SystemState contains all the state required by the OTA service.
//
// SystemOTA maintains information about the state of the system necessary
// during the update transaction. State can be marshaled to an INI file and must
// be stored persistently in a location that is immune to data loss during
// rollback, when the update transaction is cancelled.
//
// SystemState does not duplicate information for which the authoritative source
// is the boot.Protocol.
type SystemState struct {
	BootMode boot.Mode `ini:"BootMode,[System],omitempty"`

	InstalledPackageName    string `ini:"PackageName,[Installed],omitempty"`
	InstalledPackageVersion string `ini:"PackageVersion,omitempty"`
	InstalledSourceRevision string `ini:"SourceRevision,omitempty"`

	PendingPackageName    string `ini:"PackageName,[Pending],omitempty"`
	PendingPackageVersion string `ini:"PackageVersion,omitempty"`
	PendingSourceRevision string `ini:"SourceRevision,omitempty"`
}

const (
	// StateFileName is the name of the SystemOTA state file.
	//
	// This location cannot be changed easily, at least not without special
	// coordination. Old versions of sysotad will use it on roll-back. Without
	// an epoch-like system, where certain upgrade path is guaranteed, making
	// sure that old systems transition through an intermediate version that
	// performs the migration and locks out rollback, changing this path will
	// lead to broken systems.
	StateFileName = "/var/lib/sysota/state.ini"
)

// LoadState loads the SystemOTA state from the given file.
//
// The file may be missing without causing an error. In such case the zero value
// state is returned.
func LoadState(name string) (state *SystemState, err error) {
	defer func() {
		if err != nil {
			err = &LoadStateError{FileName: name, Err: err}
		}
	}()

	state = &SystemState{}

	f, err := os.Open(name)
	if err != nil {
		if os.IsNotExist(err) {
			return state, nil
		}

		return nil, err
	}

	defer func() {
		errutil.Forward(&err, f.Close())
	}()

	dec := ini.NewDecoder(f)
	if err := dec.Decode(state); err != nil {
		return nil, err
	}

	return state, nil
}

// LoadStateError records an error with loading the state.
type LoadStateError struct {
	FileName string
	Err      error
}

// Unwrap returns the error wrapped inside the load state error.
func (e *LoadStateError) Unwrap() error {
	return e.Err
}

// Error implements the error interface.
func (e *LoadStateError) Error() string {
	var pathErr *os.PathError

	if errors.As(e.Err, &pathErr) {
		// Omit the path since PathError repeats it.
		return fmt.Sprintf("cannot load state: %v", e.Err)
	}

	return fmt.Sprintf("cannot load state from %s: %v", e.FileName, e.Err)
}

// SaveState saves given SystemOTA state to the given file.
//
// The file is not written to directly, instead a temporary file is created in
// the same directory, written to and then renamed.
//
// The entire state directory should be created on the same filesystem to
// support race-free, atomic operation. The state file should not be
// bind-mounted.
func SaveState(state *SystemState, name string) (err error) {
	defer func() {
		if err != nil {
			err = &SaveStateError{Err: err}
		}
	}()

	f, err := atomicfile.CreateTemp(filepath.Dir(name), "tmp-*")
	if err != nil {
		return err
	}

	defer func() {
		errutil.Forward(&err, f.Close(), f.RemoveUnlessRenamed())
	}()

	if err := ini.NewEncoder(f).Encode(state); err != nil {
		return err
	}

	return f.AtomicRename(name)
}

// SaveStateError records an error with saving the state.
type SaveStateError struct {
	Err error
}

// Unwrap returns the error wrapped inside the save state error.
func (e *SaveStateError) Unwrap() error {
	return e.Err
}

// Error implements the error interface.
func (e *SaveStateError) Error() string {
	return fmt.Sprintf("cannot save state: %v", e.Err)
}

// StateFile is an interface for loading and saving system state.
//
// This interface bridges the outside world to a StateGuard.
type StateFile interface {
	LoadState() (*SystemState, error)
	SaveState(*SystemState) error
}

// StateGuard manages and persists system state.
//
// The zero value of StateGuard holds the zero value of the SystemState and is
// safe to use. Any changes made to the state using either LoadState, AlterState
// or SetState are persisted if state file is bound with BindAndLoad. All
// changes to the state may be observed and vetoed by state observers, see
// RegisterObserver for explanation.
//
// Do not copy StateGuard as it contains an embedded mutex.
type StateGuard struct {
	m sync.RWMutex

	sf StateFile
	st SystemState

	observers map[interface{}]func(*SystemState) error
}

// BindAndLoad binds to a given state file and immediately loads the current state.
func (g *StateGuard) BindAndLoad(sf StateFile) error {
	g.m.Lock()
	defer g.m.Unlock()

	g.sf = sf

	return g.loadUnlocked()
}

// RegisterObserver adds a function which can observe or veto any state changes.
//
// State observers should be registered with a key made from a value of
// unexported type, similar to how keys used by context.WithValue. Registering
// multiple observers with the same key overwrites the previous observer.
//
// State observers have veto capability. State cannot be set, loaded or altered
// to a value that an observer disagrees with. Observers are called in undefined
// order.
func (g *StateGuard) RegisterObserver(key interface{}, fn func(*SystemState) error) {
	g.m.Lock()
	defer g.m.Unlock()

	if g.observers == nil {
		g.observers = make(map[interface{}]func(*SystemState) error)
	}

	g.observers[key] = fn
}

// RemoveObserver removes an observed that was added earlier.
//
// It is safe to use a key that was not registered yet.
func (g *StateGuard) RemoveObserver(key interface{}) {
	g.m.Lock()
	defer g.m.Unlock()

	delete(g.observers, key)

	if len(g.observers) == 0 {
		g.observers = nil
	}
}

// StateFile returns the bound state file.
func (g *StateGuard) StateFile() StateFile {
	g.m.RLock()
	defer g.m.RUnlock()

	return g.sf
}

// State returns a copy of the state.
func (g *StateGuard) State() SystemState {
	g.m.RLock()
	defer g.m.RUnlock()

	return g.st
}

// LoadState atomically loads and replaces the state.
//
// Registered observers can reject the new state by returning an error.
func (g *StateGuard) LoadState() error {
	g.m.Lock()
	defer g.m.Unlock()

	return g.loadUnlocked()
}

// SetState atomically replaces the entire state.
//
// Registered observers can reject the new state by returning an error.
//
// State is persisted if a StateFile is bound. Any error returned from SaveState
// is returned but in-memory state is not reverted.
func (g *StateGuard) SetState(st *SystemState) error {
	g.m.Lock()
	defer g.m.Unlock()

	if err := g.alterUnlockedNoSave(st); err != nil {
		return err
	}

	// If saving becomes conditional on change add a flag to prevent earlier IO
	// error and the apparent "no-changes" optimization from saving the state.
	return g.saveUnlocked()
}

// AlterState atomically modifies the state.
//
// The helper function must return true to indicate that changes were made, or
// any changes are discarded. The helper function can fail, this error is
// propagated to the caller.
//
// Registered observers can reject the new state by returning an error.
func (g *StateGuard) AlterState(f func(st *SystemState) (bool, error)) error {
	g.m.Lock()
	defer g.m.Unlock()

	st := g.st

	if ok, err := f(&st); err != nil || !ok {
		return err
	}

	if err := g.alterUnlockedNoSave(&st); err != nil {
		return err
	}

	// If saving becomes conditional on change add a flag to prevent earlier IO
	// error and the apparent "no-changes" optimization from saving the state.
	return g.saveUnlocked()
}

func (g *StateGuard) loadUnlocked() error {
	if g.sf == nil {
		return nil
	}

	st, err := g.sf.LoadState()
	if err != nil {
		return err
	}

	return g.alterUnlockedNoSave(st)
}

func (g *StateGuard) saveUnlocked() error {
	if g.sf == nil {
		return nil
	}

	return g.sf.SaveState(&g.st)
}

func (g *StateGuard) alterUnlockedNoSave(st *SystemState) error {
	if g.st == *st {
		return nil
	}

	for _, o := range g.observers {
		if err := o(st); err != nil {
			return err
		}
	}

	g.st = *st

	return nil
}

// MemoryStateFile implements StateFile with an in-memory copy.
type MemoryStateFile struct {
	SystemState *SystemState
}

func (msf *MemoryStateFile) LoadState() (*SystemState, error) {
	return msf.SystemState, nil
}

func (msf *MemoryStateFile) SaveState(st *SystemState) error {
	msf.SystemState = st

	return nil
}
