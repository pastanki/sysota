// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

package ota_test

import (
	"errors"
	"io/ioutil"
	"os"
	"path/filepath"
	"runtime"

	. "gopkg.in/check.v1"

	"gitlab.com/zygoon/sysota/boot"
	"gitlab.com/zygoon/sysota/ota"
)

type stateSuite struct {
	name string
}

var _ = Suite(&stateSuite{})

func (s *stateSuite) SetUpTest(c *C) {
	d := c.MkDir()
	s.name = filepath.Join(d, "state.ini")
}

func (s *stateSuite) TestLoadStateWithoutStateFile(c *C) {
	state, err := ota.LoadState(s.name)
	c.Assert(err, IsNil)
	c.Check(state.BootMode, Equals, boot.Normal)
}

func (s *stateSuite) TestLoadStateWithStateFile(c *C) {
	err := ioutil.WriteFile(s.name, []byte(`
[System]
BootMode=try
`), 0600)
	c.Assert(err, IsNil)

	state, err := ota.LoadState(s.name)
	c.Assert(err, IsNil)
	c.Check(state.BootMode, Equals, boot.Try)
}

func (s *stateSuite) TestLoadStateWithCorruptedStateFile(c *C) {
	err := ioutil.WriteFile(s.name, []byte(`[System`), 0600)
	c.Assert(err, IsNil)

	_, err = ota.LoadState(s.name)
	c.Assert(err, ErrorMatches, `cannot load state from .*[/\\]state.ini: cannot decode line 1: .*`)
}

func (s *stateSuite) TestLoadStateWithBrokenStateFile(c *C) {
	if runtime.GOOS == "windows" {
		c.Skip("cannot run on Windows, test depends on symlinks which are privileged on the platform")
	}

	err := os.Symlink(s.name, s.name)
	c.Assert(err, IsNil)

	_, err = ota.LoadState(s.name)
	c.Assert(err, ErrorMatches, `cannot load state: open .*[/\\]state.ini: too many levels of symbolic links`)

	c.Check(errors.Unwrap(err), ErrorMatches, `open .*: too many levels of symbolic links`)
}

func (s *stateSuite) TestSaveStateEmpty(c *C) {
	err := ota.SaveState(&ota.SystemState{}, s.name)
	c.Assert(err, IsNil)

	data, err := ioutil.ReadFile(s.name)
	c.Assert(err, IsNil)
	c.Check(string(data), Equals, "")
}

func (s *stateSuite) TestSaveStateNonEmpty(c *C) {
	err := ota.SaveState(&ota.SystemState{BootMode: boot.Try}, s.name)
	c.Assert(err, IsNil)

	data, err := ioutil.ReadFile(s.name)
	c.Assert(err, IsNil)
	c.Check(string(data), Equals, "[System]\nBootMode=try\n")
}

func (s *stateSuite) TestSaveStateFinalPermission(c *C) {
	err := ota.SaveState(&ota.SystemState{}, s.name)
	c.Assert(err, IsNil)

	fi, err := os.Stat(s.name)
	c.Assert(err, IsNil)
	c.Check(fi.Mode(), Equals, os.FileMode(0o600))
}

func (s *stateSuite) TestSaveStateOverwrites(c *C) {
	c.Assert(ioutil.WriteFile(s.name, []byte("[System]\nBootMode=potato\n"), 0600), IsNil)

	err := ota.SaveState(&ota.SystemState{BootMode: boot.Try}, s.name)
	c.Assert(err, IsNil)

	data, err := ioutil.ReadFile(s.name)
	c.Assert(err, IsNil)
	c.Check(string(data), Equals, "[System]\nBootMode=try\n")
}

func (s *stateSuite) TestSaveStateCannotRename(c *C) {
	// Create a directory where the state file would otherwise be.
	c.Assert(os.Mkdir(s.name, 0600), IsNil)
	err := ota.SaveState(&ota.SystemState{BootMode: boot.Try}, s.name)
	c.Assert(err, ErrorMatches, `cannot save state: rename .* .*[/\\]state.ini: .*`)
	c.Assert(errors.Unwrap(err), ErrorMatches, `rename .*`)

	// Only the "blocker" directory is present.
	entries, err := ioutil.ReadDir(filepath.Dir(s.name))
	c.Assert(err, IsNil)
	c.Assert(entries, HasLen, 1)
	c.Check(entries[0].Name(), Equals, filepath.Base(s.name))
	c.Check(entries[0].IsDir(), Equals, true)
}

func (s *stateSuite) TestSaveStateCannotMarshal(c *C) {
	err := ota.SaveState(&ota.SystemState{BootMode: boot.Mode(42)}, s.name)
	c.Assert(err, ErrorMatches, `cannot save state: cannot encode SystemState.BootMode: invalid boot mode`)

	// No temporary files present
	entries, err := ioutil.ReadDir(filepath.Dir(s.name))
	c.Assert(err, IsNil)
	c.Assert(entries, HasLen, 0)
}

func (s *stateSuite) TestSaveStateCannotMarshalDoesNotClobberOldState(c *C) {
	c.Assert(ioutil.WriteFile(s.name, []byte("[System]\nBootMode=potato\n"), 0600), IsNil)

	err := ota.SaveState(&ota.SystemState{BootMode: boot.Mode(42)}, s.name)
	c.Assert(err, ErrorMatches, `cannot save state: cannot encode SystemState.BootMode: invalid boot mode`)

	data, err := ioutil.ReadFile(s.name)
	c.Assert(err, IsNil)
	c.Check(string(data), Equals, "[System]\nBootMode=potato\n")
}

var errBoom = errors.New("boom")

var _ = Suite(&stateGuardSuite{})

type stateGuardSuite struct {
	guard ota.StateGuard

	// For LoadState and SaveState.
	saved *ota.SystemState
	ioErr error
}

func (s *stateGuardSuite) SetUpTest(c *C) {
	s.ioErr = nil
	s.saved = &ota.SystemState{}

	c.Assert(s.guard.BindAndLoad(s), IsNil)

	s.saved = nil
}

func (s *stateGuardSuite) SaveState(st *ota.SystemState) error {
	if s.ioErr != nil {
		return s.ioErr
	}

	s.saved = st

	return nil
}

func (s *stateGuardSuite) LoadState() (*ota.SystemState, error) {
	if s.ioErr != nil {
		return nil, s.ioErr
	}

	return s.saved, nil
}

func (s *stateGuardSuite) TestStateFile(c *C) {
	c.Check(s.guard.StateFile(), Equals, s)
}

func (s *stateGuardSuite) TestGetSetState(c *C) {
	s.saved = nil
	c.Check(s.guard.State(), Equals, ota.SystemState{})
	c.Assert(s.guard.SetState(&ota.SystemState{BootMode: boot.Try}), IsNil)
	c.Check(s.guard.State(), Equals, ota.SystemState{BootMode: boot.Try})
	c.Check(s.saved, DeepEquals, &ota.SystemState{BootMode: boot.Try})
}

func (s *stateGuardSuite) TestLoadStateIOError(c *C) {
	s.saved = &ota.SystemState{BootMode: boot.Try}
	s.ioErr = errBoom

	c.Assert(s.guard.LoadState(), ErrorMatches, "boom")
	c.Check(s.guard.State(), Equals, ota.SystemState{BootMode: boot.Normal})
}

func (s *stateGuardSuite) TestLoadState(c *C) {
	s.saved = &ota.SystemState{BootMode: boot.Try}
	c.Check(s.guard.LoadState(), IsNil)
	c.Check(s.guard.State(), Equals, ota.SystemState{BootMode: boot.Try})
}

func (s *stateGuardSuite) TestSaveStateIOError(c *C) {
	s.ioErr = errBoom

	// Failures are reported but do not cause the in-memory change to be discarded.
	c.Assert(s.guard.SetState(&ota.SystemState{BootMode: boot.Try}), ErrorMatches, "boom")
	c.Check(s.saved, IsNil)
	c.Check(s.guard.State(), Equals, ota.SystemState{BootMode: boot.Try})
	c.Check(s.saved, IsNil)

	// If we retry successfully without any actual change, we still get saved.
	s.ioErr = nil
	c.Assert(s.guard.SetState(&ota.SystemState{BootMode: boot.Try}), IsNil)
	c.Check(s.saved, DeepEquals, &ota.SystemState{BootMode: boot.Try})
}

func (s *stateGuardSuite) TestAlterStateSuccess(c *C) {
	err := s.guard.AlterState(func(st *ota.SystemState) (bool, error) {
		st.BootMode = boot.Try

		return true, nil
	})

	c.Assert(err, IsNil)
	c.Check(s.guard.State(), Equals, ota.SystemState{BootMode: boot.Try})
	c.Check(s.saved, DeepEquals, &ota.SystemState{BootMode: boot.Try})
}

type stateObserverKey struct{}

func (s *stateGuardSuite) TestObserver(c *C) {
	s.guard.RegisterObserver(stateObserverKey{}, func(st *ota.SystemState) error {
		c.Assert(st.BootMode, Equals, boot.Try)
		return nil
	})
	defer s.guard.RemoveObserver(stateObserverKey{})

	err := s.guard.AlterState(func(st *ota.SystemState) (bool, error) {
		st.BootMode = boot.Try

		return true, nil
	})

	c.Assert(err, IsNil)
	c.Check(s.guard.State(), Equals, ota.SystemState{BootMode: boot.Try})
	c.Check(s.saved, DeepEquals, &ota.SystemState{BootMode: boot.Try})
}

func (s *stateGuardSuite) TestObserverError(c *C) {
	s.guard.RegisterObserver(stateObserverKey{}, func(st *ota.SystemState) error {
		c.Assert(st.BootMode, Equals, boot.Try)
		return errBoom
	})
	defer s.guard.RemoveObserver(stateObserverKey{})

	err := s.guard.AlterState(func(st *ota.SystemState) (bool, error) {
		st.BootMode = boot.Try

		return true, nil
	})

	c.Assert(err, ErrorMatches, "boom")
	c.Check(s.guard.State(), Equals, ota.SystemState{BootMode: boot.Normal})
	c.Check(s.saved, IsNil)
}

func (s *stateGuardSuite) TestAlterStateFailure(c *C) {
	err := s.guard.AlterState(func(st *ota.SystemState) (bool, error) {
		st.BootMode = boot.Try

		return false, errBoom
	})

	c.Assert(err, ErrorMatches, "boom")
	c.Check(s.guard.State(), Equals, ota.SystemState{BootMode: boot.Normal})
	c.Check(s.saved, IsNil)
}

func (s *stateGuardSuite) TestAlterStateUnchanged(c *C) {
	err := s.guard.AlterState(func(st *ota.SystemState) (bool, error) {
		st.BootMode = boot.Try

		return false, nil
	})

	c.Assert(err, IsNil)
	c.Check(s.guard.State(), Equals, ota.SystemState{BootMode: boot.Normal})
	c.Check(s.saved, IsNil)
}

func (s *stateGuardSuite) TestZeroValue(c *C) {
	var g ota.StateGuard // state file is not bound

	c.Check(g.State(), Equals, ota.SystemState{})
	c.Assert(g.SetState(&ota.SystemState{BootMode: boot.Try}), IsNil)
	c.Check(g.AlterState(func(*ota.SystemState) (bool, error) { return true, nil }), IsNil)
}
