// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

// Package dbustest contains helpers useful for testing code interacting with D-Bus.
package dbustest

import (
	"bufio"
	"fmt"
	"io/ioutil"
	"os"
	"os/exec"
	"path/filepath"
	"runtime"
	"text/template"

	"gopkg.in/check.v1"
)

// TestBus provides a D-Bus bus useful for testing.
type TestBus struct {
	dbusDaemon *exec.Cmd
	busAddr    string
	tempDir    string
}

// NewTestBus starts a dbus-daemon process and returns a new TestBus.
func NewTestBus() (testBus *TestBus, err error) {
	dbusDaemonCmd, err := exec.LookPath("dbus-daemon")
	if err != nil {
		return nil, err
	}

	// Create D-Bus configuration file allowing all communication and setting a custom UNIX socket path.
	tempDir, err := ioutil.TempDir(os.TempDir(), "test-bus-*")
	if err != nil {
		return nil, err
	}

	defer func() {
		if err != nil {
			_ = os.RemoveAll(tempDir)
		}
	}()

	cfgFileName := filepath.Join(tempDir, "system.conf")

	cfgFile, err := os.OpenFile(cfgFileName, os.O_CREATE|os.O_TRUNC|os.O_WRONLY, 0600)
	if err != nil {
		return nil, err
	}

	cfgFileTemplate := template.Must(template.New("").Parse(`
	<busconfig>
		<type>system</type>
		<listen>unix:path={{.TempDir}}/system_bus_socket</listen>
		<auth>EXTERNAL</auth>
		<policy context="default">
			<allow send_destination="*" eavesdrop="true"/>
			<allow eavesdrop="true"/>
			<allow own="*"/>
		</policy>
	  </busconfig>
  	`))

	if err := cfgFileTemplate.Execute(cfgFile, struct{ TempDir string }{TempDir: tempDir}); err != nil {
		return nil, err
	}

	// Launch D-Bus daemon handling traffic on our test bus.
	dbusDaemon := exec.Command(
		dbusDaemonCmd, "--config-file="+cfgFileName,
		"--nosyslog", "--print-address", "--nofork")
	dbusDaemon.Stderr = os.Stderr

	pout, err := dbusDaemon.StdoutPipe()
	if err != nil {
		return nil, err
	}

	if err := dbusDaemon.Start(); err != nil {
		return nil, err
	}

	runtime.SetFinalizer(dbusDaemon, func(cmd *exec.Cmd) {
		_ = cmd.Process.Kill()
	})

	defer func() {
		if err != nil {
			_ = dbusDaemon.Process.Kill()
			_, _ = dbusDaemon.Process.Wait()
		}
	}()

	// Parse the advertised DBUS_SYSTEM_BUS_ADDRESS.
	scanner := bufio.NewScanner(pout)
	scanner.Scan()

	if err := scanner.Err(); err != nil {
		return nil, err
	}

	busAddr := scanner.Text()

	testBus = &TestBus{
		dbusDaemon: dbusDaemon,
		busAddr:    busAddr,
		tempDir:    tempDir,
	}

	return testBus, nil
}

// BusAddress returns the address of the test bus in a format compatible with D-Bus.
func (testBus *TestBus) BusAddress() string {
	return testBus.busAddr
}

// Close stops the dbus-daemon process and removes the temporary configuration file.
func (testBus *TestBus) Close() error {
	if err := testBus.dbusDaemon.Process.Kill(); err != nil {
		return err
	}

	if _, err := testBus.dbusDaemon.Process.Wait(); err != nil {
		return err
	}

	return os.RemoveAll(testBus.tempDir)
}

// SetDBusSystemBusAddress sets the address of the D-Bus system bus, returning an undo function.
//
// The function sets an OS-specific environment variable in the current process.
func SetDBusSystemBusAddress(addr string) (restore func() error, err error) {
	const varName = systemBusPathEnvVar
	old, ok := os.LookupEnv(varName)

	if err := os.Setenv(varName, addr); err != nil {
		return nil, err
	}

	restore = func() error {
		if ok {
			return os.Setenv(varName, old)
		}

		return os.Unsetenv(varName)
	}

	return restore, nil
}

// Suite is a test which provides a test D-Bus system bus.
type Suite struct {
	testBus    *TestBus
	restoreEnv func() error
}

// SetUpSuite starts a new TestBus and sets the system bus address.
func (s *Suite) SetUpSuite(c *check.C) {
	testBus, err := NewTestBus()
	if err != nil {
		c.Skip(fmt.Sprintf("cannot initialize TestBus: %v", err))
	}

	s.testBus = testBus

	restoreEnv, err := SetDBusSystemBusAddress(testBus.BusAddress())
	c.Assert(err, check.IsNil)

	s.restoreEnv = restoreEnv
}

// TearDownSuite stops TestBus and resets system bus address.
func (s *Suite) TearDownSuite(c *check.C) {
	if s.restoreEnv != nil {
		err := s.restoreEnv()
		c.Assert(err, check.IsNil)
	}

	if s.testBus != nil {
		err := s.testBus.Close()
		c.Assert(err, check.IsNil)
	}
}
