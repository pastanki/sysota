// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

package dbusutil

import (
	"context"
	"errors"
	"fmt"
	"log"
	"sync"

	"github.com/godbus/dbus/v5"
)

// WatchedObject is a D-Bus object whose properties are cached locally.
//
// The implementation relies on the org.freedesktop.DBus.Properties interface.
// The object is expected to send the PropertiesChanged signal and to implement
// the GetAll method.
//
// See NewWatchedObject for instructions on how to use this type safely.
type WatchedObject struct {
	m          sync.RWMutex
	cached     map[string]map[string]dbus.Variant
	changes    chan string
	changeLost chan struct{}
	done       chan struct{}
}

// NewWatchedObject establishes monitoring of the given D-Bus object.
//
// All the property changes in the object are reflected locally and can be
// retrieved without incurring additional latency of D-Bus calls. Monitoring
// ceases when the context is cancelled.
//
// Callers should adopt the following structure:
//
//     conn, err := dbus.SessionBusPrivate(dbus.WithSignalHandler(dbus.NewSequentialSignalHandler()))
//     if err != nil {
//         /* error handling */
//     }
//
//     ctx, cancel := context.WithCancel(ctx)
//     /* make sure to cancel the context at some point to remove the D-Bus watch */
//     defer cancel()
//
//     w, err := NewWatchedObject(ctx, conn, busName, path, iface1, iface2)
//     if err != nil {
//         /* error handling */
//     }
//
//     for /* loop termination condition (e.g. something is marked as done) */ {
//         select {
//             case iface <- w.Changes(): /* changes are not buffered */
//                 fmt.Printf("properties on interface %s have changed\n", iface)
//             case <-w.ChangeLost(): /* change lost has a buffer dept of one */
//                 fmt.Printf("properties have changed while we were busy\n")
//         }
//     }
//
//     <- w.Done() /* Wait for monitoring shut-down. */
//
//     /* Updates have ceased and final state is available for inspection. */
//
// This guarantees that the terminating condition is never lost. It is
// important to check before falling asleep for changes, as the changes may
// never arrive if the watched object has reached a "final" state before the
// watch was established.
func NewWatchedObject(ctx context.Context, conn *dbus.Conn, busName string, path dbus.ObjectPath, ifaces ...string) (w *WatchedObject, err error) {
	// Subscribe to all the signals originating from the operation object.
	matchOpts := []dbus.MatchOption{
		dbus.WithMatchSender(busName),
		dbus.WithMatchObjectPath(path),
		dbus.WithMatchInterface(PropertiesInterface),
		dbus.WithMatchMember(PropertiesChangedSignal),
	}

	if err := conn.AddMatchSignalContext(ctx, matchOpts...); err != nil {
		return nil, err
	}

	// In case something fails below, remove the subscription before we return.
	defer func() {
		if err != nil {
			_ = conn.RemoveMatchSignalContext(ctx, matchOpts...)
		}
	}()

	// Set a channel for streaming D-Bus signals. The channel is automatically
	// closed when it is removed from the bus. It is important that this channel
	// is set *before* we query for all the properties, this way we can ensure
	// that no notification is lost in case the object changes as we are still
	// busy retrieving initial state.
	sigCh := make(chan *dbus.Signal)
	conn.Signal(sigCh)

	// In case something fails below, remove the signal channel.
	defer func() {
		if err != nil {
			conn.RemoveSignal(sigCh)
		}
	}()

	w = &WatchedObject{
		cached: make(map[string]map[string]dbus.Variant),
		// Buffering of those two channels is very important. See the
		// description of the NewWatchedObject for rationale.
		changes:    make(chan string),
		changeLost: make(chan struct{}, 1),
		done:       make(chan struct{}),
	}

	// Get initial set of properties for each interface we are interested in.
	// This cannot be done atomically if multiple interfaces are to be observed
	// but this is fine, as the goroutine below will catch up to any state
	// changes that are pending.
	if err := w.warmUpCache(ctx, conn, busName, path, ifaces); err != nil {
		return nil, err
	}

	// Start reading incoming D-Bus signals which update the property cache.
	go w.monitor(ctx, conn, sigCh, func() {
		// The context we are given may have been cancelled, but we need to
		// perform additional blocking operations to tell the bus to refrain
		// from sending us additional signals.
		//
		// Use the connection context to perform this operation.
		if err := conn.RemoveMatchSignalContext(conn.Context(), matchOpts...); err != nil {
			if !errors.Is(context.Canceled, err) {
				log.Printf("Cannot remove signal match: %v\n", err)
			}
		}

		// Remove and close the signal notification channel.
		conn.RemoveSignal(sigCh)

		// To avoid raciness in go-dbus, when the property observation is over,
		// perform one final GetAll to synchronize properties. Having done that,
		// tell the client that a change was lost (unless the client already has
		// such event pending).
		//
		// This avoids a bug in go-dbus where prop.Properties cannot synchronize
		// PropertiesChanged emission with Get and GetAll return message.
		//
		// https://twitter.com/zygoon/status/1477284637500182535
		//
		// Use the connection context to perform this operation.
		if err := w.warmUpCache(conn.Context(), conn, busName, path, ifaces); err != nil {
			if !errors.Is(context.Canceled, err) {
				log.Printf("Cannot perform cached property update: %v\n", err)
			}
		}

		w.notifyChangeLost()

		close(w.changes)
		close(w.changeLost)
		close(w.done)
	})

	return w, nil
}

// Done returns a channel closed when monitoring shutdown is complete.
func (w *WatchedObject) Done() <-chan struct{} {
	return w.done
}

func (w *WatchedObject) notifyChangeLost() {
	select {
	case w.changeLost <- struct{}{}:
	default:
		// Change lost channel is full, so we already have a
		// pending notification and can safely discard this one.
		//
		// The break statement is redundant but makes go-wsl
		// happy about the comment above.
		break
	}
}

// warmUpCache retrieves the initial value of properties for given interfaces.
func (w *WatchedObject) warmUpCache(ctx context.Context, conn *dbus.Conn, busName string, path dbus.ObjectPath, ifaces []string) error {
	for _, iface := range ifaces {
		call := conn.Object(busName, path).CallWithContext(
			ctx, PropertiesInterface+"."+PropertyGetAllMethod, 0, iface)

		var ifaceProps map[string]dbus.Variant

		if err := call.Store(&ifaceProps); err != nil {
			return err
		}

		w.m.Lock()
		w.cached[iface] = ifaceProps
		w.m.Unlock()
	}

	return nil
}

// monitor watches for property update signal and updates the cache.
func (w *WatchedObject) monitor(ctx context.Context, conn *dbus.Conn, sigCh chan *dbus.Signal, shutdown func()) {
	defer shutdown()

	for {
		select {
		case <-ctx.Done():
			// Return when the context is cancelled.
			return
		case sig := <-sigCh:
			// When sigCh is closed we receive the zero value, namely a nil signal.
			if sig == nil {
				continue
			}

			// Process the signal, this updates property cache.
			iface, err := w.processSignal(sig)
			if err != nil {
				log.Printf("Cannot process D-Bus signal: %v\n", err)
				continue
			}

			// Notify observers that interface properties have changed.
			select {
			case w.changes <- iface:
			default:
				// Nobody is ready to notice the change, remember a strobe was lost.
				w.notifyChangeLost()
			}
		}
	}
}

// processSignal updates the property cache in response to the properties changed signal.
func (w *WatchedObject) processSignal(sig *dbus.Signal) (iface string, err error) {
	// Look only at the PropertiesChanged signals.
	if sig.Name != PropertiesInterface+"."+PropertiesChangedSignal {
		return "", nil
	}

	iface, changed, invalided, err := decodePropsChanged(sig)
	if err != nil {
		return "", fmt.Errorf("cannot decode signal %s: %v", sig.Name, err)
	}

	// Update local copy of all the properties while holding the
	// write-lock. This way callers of CachedProperty will see a
	// consistent view at the granularity of once interface.
	w.m.Lock()
	defer w.m.Unlock()

	ifaceProps := w.cached[iface]
	if ifaceProps == nil {
		ifaceProps = make(map[string]dbus.Variant)
		w.cached[iface] = ifaceProps
	}

	for name, value := range changed {
		ifaceProps[name] = value
	}

	for _, name := range invalided {
		delete(ifaceProps, name)
	}

	return iface, nil
}

// CachedProperty returns a cached copy of a property.
//
// Invalidated properties are not cached. To obtain their value access them
// explicitly. To access multiple cached properties coherently use
// CachedProperties instead.
func (w *WatchedObject) CachedProperty(iface, name string) dbus.Variant {
	w.m.RLock()
	defer w.m.RUnlock()

	return w.cached[iface][name]
}

// CachedProperties returns a slice of cached properties.
//
// The returned value is guaranteed to be in a consistent state in face of
// concurrent updates. Distinct interfaces are never coherent, even if they
// are provided by the same object.
func (w *WatchedObject) CachedProperties(iface string, names ...string) []dbus.Variant {
	w.m.RLock()
	defer w.m.RUnlock()

	values := make([]dbus.Variant, 0, len(names))
	for _, name := range names {
		values = append(values, w.cached[iface][name])
	}

	return values
}

// Changes returns a channel notifying about interface property changes.
//
// The returned channel is not buffered. This way regardless of the rate of
// changes and the speed of the observer the amount of used memory is bounded.
//
// See NewWatchedObject for instructions on how to use this channel safely.
func (w *WatchedObject) Changes() <-chan string {
	return w.changes
}

// ChangeLost returns a channel notifying that at least one change notification was lost.
//
// This channel strobes when a change was received over D-Bus signal, the
// properties were updated but there were no observers ready to act on them by
// reading from the channel returned by Changes.
//
// The returned channel has a buffer depth of one, guaranteeing that at least
// one notification about lost changes is observed.
//
// See NewWatchedObject for instructions on how to use this channel safely.
func (w *WatchedObject) ChangeLost() <-chan struct{} {
	return w.changeLost
}

var errInvalidSignalFormat = errors.New("invalid signal format")

func decodePropsChanged(sig *dbus.Signal) (iface string, changed map[string]dbus.Variant, invalidated []string, err error) {
	if len(sig.Body) != 3 {
		return "", nil, nil, errInvalidSignalFormat
	}

	var ok bool

	iface, ok = sig.Body[0].(string)
	if !ok {
		return "", nil, nil, errInvalidSignalFormat
	}

	changed, ok = sig.Body[1].(map[string]dbus.Variant)
	if !ok {
		return "", nil, nil, errInvalidSignalFormat
	}

	invalidated, ok = sig.Body[2].([]string)
	if !ok {
		return "", nil, nil, errInvalidSignalFormat
	}

	return iface, changed, invalidated, nil
}
