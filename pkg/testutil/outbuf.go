// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

package testutil

import (
	"bytes"
	"context"
	"os"

	"gitlab.com/zygoon/go-cmdr"
)

// BufferOutput returns a context with stdout and stderr replaced by byte buffers.
//
// The returned context is useful for testing the output of well-behaved
// cmdr.Cmd implementations which retrieve IO streams from the context.
func BufferOutput(ctx context.Context) (ctx2 context.Context, stdout, stderr *bytes.Buffer) {
	stdout = new(bytes.Buffer)
	stderr = new(bytes.Buffer)

	return cmdr.WithStdio(ctx, os.Stdin, stdout, stderr), stdout, stderr
}
