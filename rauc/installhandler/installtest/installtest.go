// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

// Package installtest contains test implementation of installtest.Interface.
package installtest

import (
	"sync"

	"gitlab.com/zygoon/sysota/rauc/installhandler"
)

// Handler implements an installhandler.Interface suitable for testing.
type Handler struct {
	m sync.RWMutex

	preInstallFn  func(env *installhandler.Environment) error
	postInstallFn func(env *installhandler.Environment) error
}

// MockPreInstallFn sets the pre install handler function.
func (h *Handler) MockPreInstallFn(fn func(env *installhandler.Environment) error) {
	h.m.Lock()
	defer h.m.Unlock()

	h.preInstallFn = fn
}

// MockPostInstallFn sets the post install handler function.
func (h *Handler) MockPostInstallFn(fn func(env *installhandler.Environment) error) {
	h.m.Lock()
	defer h.m.Unlock()

	h.postInstallFn = fn
}

// ResetCallbacks resets all callback functions to nil.
func (h *Handler) ResetCallbacks() {
	h.m.Lock()
	defer h.m.Unlock()

	h.preInstallFn = nil
	h.postInstallFn = nil
}

// PreInstall calls the PreInstallFn callback function.
func (h *Handler) PreInstall(env *installhandler.Environment) error {
	h.m.RLock()
	defer h.m.RUnlock()

	if h.preInstallFn == nil {
		panic("please provide PreInstallFn callback function")
	}

	return h.preInstallFn(env)
}

// PostInstall calls the PostInstallFn callback function.
func (h *Handler) PostInstall(env *installhandler.Environment) error {
	h.m.RLock()
	defer h.m.RUnlock()

	if h.postInstallFn == nil {
		panic("please provide PostInstallFn callback function")
	}

	return h.postInstallFn(env)
}
